#pragma comment(lib, "winmm.lib")
#include <cmath>
#include <iostream>
#include <GL/glew.h>
#include <GL/glut.h>
#include "Player.h"
#include "Game.h"
#include <windows.h>
#include <mmsystem.h>


#define JUMP_ANGLE_STEP 2
#define JUMP_HEIGHT 96
#define FALL_STEP 3
#define SPRITE 0.05f
#define SPRITE_Y 0.025f


enum PlayerAnims
{
	STAND_LEFT, STAND_RIGHT, MOVE_LEFT, MOVE_RIGHT, MOVE_LEFT_SLOW, MOVE_RIGHT_SLOW,
	STOP_LEFT, STOP_RIGHT, CHANGE_TO_LEFT, CHANGE_TO_RIGHT, JUMP_STAND_LEFT, JUMP_STAND_RIGHT,
	RUN_LEFT, RUN_RIGHT, RUN_CHANGE_TO_LEFT, RUN_CHANGE_TO_RIGHT, JUMP_STATIC_LEFT, JUMP_STATIC_RIGHT,
	CLIMB_LEFT, CLIMB_RIGHT, SQUAT_LEFT, SQUAT_RIGHT, STANDUP_LEFT, STANDUP_RIGHT, CROUCHED_LEFT,
	CROUCHED_RIGHT, JUMP_RUN_LEFT, JUMP_RUN_RIGHT, JUMP_STATIC_LEFT_FINISH, JUMP_STATIC_RIGHT_FINISH,
	FALL_LEFT, FALL_RIGHT, CROUCHED_FALL_LEFT, CROUCHED_FALL_RIGHT, DEATH_LEFT, DEATH_RIGHT, UNSHEATHE_LEFT,
	UNSHEATHE_RIGHT, SHEATHE_LEFT, SHEATHE_RIGHT, ATTACK_LEFT, ATTACK_RIGHT, PROTECT_LEFT, PROTECT_RIGHT,
	RIPOSTE_LEFT, RIPOSTE_RIGHT, DEATH_SPIKES_LEFT, DEATH_SPIKES_RIGHT, POTION_LEFT, POTION_RIGHT, DOOR_LEFT,
	DOOR_RIGHT, DESCEND_LEFT, DESCEND_RIGHT, CLIMB_WAIT_LEFT, CLIMB_WAIT_RIGHT, SNAKE_LEFT, SNAKE_RIGHT
};

const int sprite_pos[58] = {0, 0, 6, 1, 8, 3, 7, 2, 4, 4,
							17, 5, 10, 9, 12, 11, 14, 13, 16, 15,
							19, 18, 21, 20, 19, 18, 23, 22, 14, 13, 
							24, 25, 19, 18, 27, 26, 29, 28, 31, 30,
							33, 32, 35, 34, 29, 28, 24, 25, 37, 36,
							39, 38, 16, 15, 16, 15, 25, 24 };

float pos;


void Player::init(const glm::ivec2 &tileMapPos, ShaderProgram &shaderProgram)
{
	bJumping = hasSword = unsheathe = haEscalat = false;
	canvi_lvl = false;
	spritesheet.loadFromFile("images/prince.png", TEXTURE_PIXEL_FORMAT_RGBA);
	sprite = Sprite::createSprite(glm::ivec2(64, 64), glm::vec2(SPRITE, SPRITE_Y), &spritesheet, &shaderProgram);
	sprite->setNumberAnimations(58);
	
		sprite->setAnimationSpeed(STAND_LEFT, 8);
		sprite->addKeyframe(STAND_LEFT, glm::vec2(SPRITE, sprite_pos[STAND_LEFT]*SPRITE_Y));
		sprite->setSpeed(STAND_LEFT, glm::ivec2(0, 0), 1);
		
		sprite->setAnimationSpeed(STAND_RIGHT, 8);
		sprite->addKeyframe(STAND_RIGHT, glm::vec2(0.f, sprite_pos[STAND_RIGHT] * SPRITE_Y));
		sprite->setSpeed(STAND_RIGHT, glm::ivec2(0, 0), 1);
		
		sprite->setAnimationSpeed(MOVE_LEFT, 8);
		pos = sprite_pos[MOVE_LEFT] * SPRITE_Y;
		for (int i = 5; i >= 0; --i) sprite->addKeyframe(MOVE_LEFT, glm::vec2(i*SPRITE, pos));
		sprite->setSpeed(MOVE_LEFT, glm::ivec2(0, 0), 2);
		sprite->setSpeed(MOVE_LEFT, glm::ivec2(-1, 0), 2);
		sprite->setSpeed(MOVE_LEFT, glm::ivec2(-2, 0), 2);
		
		sprite->setAnimationSpeed(MOVE_RIGHT, 8);
		pos = sprite_pos[MOVE_RIGHT] * SPRITE_Y;
		for (int i = 0; i < 6; ++i) sprite->addKeyframe(MOVE_RIGHT, glm::vec2(i*SPRITE, pos));
		sprite->setSpeed(MOVE_RIGHT, glm::ivec2(0, 0), 2);
		sprite->setSpeed(MOVE_RIGHT, glm::ivec2(1, 0), 2);
		sprite->setSpeed(MOVE_RIGHT, glm::ivec2(2, 0), 2);

		sprite->setAnimationSpeed(MOVE_LEFT_SLOW, 8);
		pos = sprite_pos[MOVE_LEFT_SLOW] * SPRITE_Y;
		for (int i = 6; i >= 0; --i) sprite->addKeyframe(MOVE_LEFT_SLOW, glm::vec2(i*SPRITE, pos));
		sprite->setSpeed(MOVE_LEFT_SLOW, glm::ivec2(0, 0), 4);
		sprite->setSpeed(MOVE_LEFT_SLOW, glm::ivec2(-1, 0), 3);

		sprite->setAnimationSpeed(MOVE_RIGHT_SLOW, 8);
		pos = sprite_pos[MOVE_RIGHT_SLOW] * SPRITE_Y;
		for (int i = 0; i < 7; ++i) sprite->addKeyframe(MOVE_RIGHT_SLOW, glm::vec2(i*SPRITE, pos));
		sprite->setSpeed(MOVE_RIGHT_SLOW, glm::ivec2(0, 0), 4);
		sprite->setSpeed(MOVE_RIGHT_SLOW, glm::ivec2(1, 0), 3);

		sprite->setAnimationSpeed(CHANGE_TO_LEFT, 8);
		pos = sprite_pos[CHANGE_TO_LEFT] * SPRITE_Y;
		for (int i = 4; i >= 0; --i) sprite->addKeyframe(CHANGE_TO_LEFT, glm::vec2(i*SPRITE, pos));
		sprite->setSpeed(CHANGE_TO_LEFT, glm::ivec2(0, 0), 5);

		sprite->setAnimationSpeed(CHANGE_TO_RIGHT, 8);
		pos = sprite_pos[CHANGE_TO_RIGHT] * SPRITE_Y;
		for (int i = 0; i < 5; ++i) sprite->addKeyframe(CHANGE_TO_RIGHT, glm::vec2(i*SPRITE, pos));
		sprite->setSpeed(CHANGE_TO_RIGHT, glm::ivec2(0, 0), 5);

		sprite->setAnimationSpeed(STOP_LEFT, 8);
		pos = sprite_pos[STOP_LEFT] * SPRITE_Y;
		for (int i = 5; i >= 0; --i) sprite->addKeyframe(STOP_LEFT, glm::vec2(i*SPRITE, pos));
		sprite->setSpeed(STOP_LEFT, glm::ivec2(-1, 0), 4);
		sprite->setSpeed(STOP_LEFT, glm::ivec2(0, 0), 2);

		sprite->setAnimationSpeed(STOP_RIGHT, 8);
		pos = sprite_pos[STOP_RIGHT] * SPRITE_Y;
		for (int i = 0; i < 5; ++i) sprite->addKeyframe(STOP_RIGHT, glm::vec2(i*SPRITE, pos));
		sprite->setSpeed(STOP_RIGHT, glm::ivec2(1, 0), 4);
		sprite->setSpeed(STOP_RIGHT, glm::ivec2(0, 0), 2);

		sprite->setAnimationSpeed(JUMP_STAND_RIGHT, 8);
		pos = sprite_pos[JUMP_STAND_RIGHT] * SPRITE_Y;
		for (int i = 0; i < 15; ++i) sprite->addKeyframe(JUMP_STAND_RIGHT, glm::vec2(i*SPRITE, pos));
		sprite->setSpeed(JUMP_STAND_RIGHT, glm::ivec2(0, 0), 5);
		sprite->setSpeed(JUMP_STAND_RIGHT, glm::ivec2(3, 0), 5);
		sprite->setSpeed(JUMP_STAND_RIGHT, glm::ivec2(0, 0), 5);

		sprite->setAnimationSpeed(JUMP_STAND_LEFT, 8);
		pos = sprite_pos[JUMP_STAND_LEFT] * SPRITE_Y;
		for (int i = 14; i >= 0; --i) sprite->addKeyframe(JUMP_STAND_LEFT, glm::vec2(i*SPRITE, pos));
		sprite->setSpeed(JUMP_STAND_LEFT, glm::ivec2(0, 0), 5);
		sprite->setSpeed(JUMP_STAND_LEFT, glm::ivec2(-3, 0), 5);
		sprite->setSpeed(JUMP_STAND_LEFT, glm::ivec2(0, 0), 5);

		sprite->setAnimationSpeed(RUN_LEFT, 8);
		pos = sprite_pos[RUN_LEFT] * SPRITE_Y;
		for (int i = 7; i >= 0; --i) sprite->addKeyframe(RUN_LEFT, glm::vec2(i*SPRITE, pos));
		sprite->setSpeed(RUN_LEFT, glm::ivec2(-2, 0), 8);

		sprite->setAnimationSpeed(RUN_RIGHT, 8);
		pos = sprite_pos[RUN_RIGHT] * SPRITE_Y;
		for (int i = 0; i < 8; ++i) sprite->addKeyframe(RUN_RIGHT, glm::vec2(i*SPRITE, pos));
		sprite->setSpeed(RUN_RIGHT, glm::ivec2(2, 0), 8);

		sprite->setAnimationSpeed(RUN_CHANGE_TO_LEFT, 8);
		pos = sprite_pos[RUN_CHANGE_TO_LEFT] * SPRITE_Y;
		for (int i = 0; i < 11; ++i) sprite->addKeyframe(RUN_CHANGE_TO_LEFT, glm::vec2(i*SPRITE, pos));
		sprite->setSpeed(RUN_CHANGE_TO_LEFT, glm::ivec2(1, 0), 6);
		sprite->setSpeed(RUN_CHANGE_TO_LEFT, glm::ivec2(0, 0), 3);
		sprite->setSpeed(RUN_CHANGE_TO_LEFT, glm::ivec2(-1, 0), 2);

		sprite->setAnimationSpeed(RUN_CHANGE_TO_RIGHT, 8);
		pos = sprite_pos[RUN_CHANGE_TO_RIGHT] * SPRITE_Y;
		for (int i = 10; i >= 0; --i) sprite->addKeyframe(RUN_CHANGE_TO_RIGHT, glm::vec2(i*SPRITE, pos));
		sprite->setSpeed(RUN_CHANGE_TO_RIGHT, glm::ivec2(-1, 0), 6);
		sprite->setSpeed(RUN_CHANGE_TO_RIGHT, glm::ivec2(0, 0), 3);
		sprite->setSpeed(RUN_CHANGE_TO_RIGHT, glm::ivec2(1, 0), 2);

		sprite->setAnimationSpeed(JUMP_STATIC_RIGHT, 8);
		sprite->setLoop(JUMP_STATIC_RIGHT, false);
		pos = sprite_pos[JUMP_STATIC_RIGHT] * SPRITE_Y;
		for (int i = 0; i < 10; ++i) sprite->addKeyframe(JUMP_STATIC_RIGHT, glm::vec2(i*SPRITE, pos));
		sprite->setSpeed(JUMP_STATIC_RIGHT, glm::ivec2(0, 0), 8);
		sprite->setSpeed(JUMP_STATIC_RIGHT, glm::ivec2(0, -3), 1);
		sprite->setSpeed(JUMP_STATIC_RIGHT, glm::ivec2(0, -1), 1);
		//sprite->setSpeed(JUMP_STATIC_RIGHT, glm::ivec2(0, 0), 2);
		
		sprite->setAnimationSpeed(JUMP_STATIC_LEFT, 8);
		sprite->setLoop(JUMP_STATIC_LEFT, false);
		pos = sprite_pos[JUMP_STATIC_LEFT] * SPRITE_Y;
		for (int i = 11; i >= 2; --i) sprite->addKeyframe(JUMP_STATIC_LEFT, glm::vec2(i*SPRITE, pos));
		sprite->setSpeed(JUMP_STATIC_LEFT, glm::ivec2(0, 0), 8);
		sprite->setSpeed(JUMP_STATIC_LEFT, glm::ivec2(0, -3), 1);
		sprite->setSpeed(JUMP_STATIC_LEFT, glm::ivec2(0, -1), 1);
		//sprite->setSpeed(JUMP_STATIC_LEFT, glm::ivec2(0, 0), 2);

		sprite->setAnimationSpeed(JUMP_STATIC_RIGHT_FINISH, 8);
		pos = sprite_pos[JUMP_STATIC_RIGHT_FINISH] * SPRITE_Y;
		for (int i = 10; i < 12; ++i) sprite->addKeyframe(JUMP_STATIC_RIGHT_FINISH, glm::vec2(i*SPRITE, pos));
		sprite->setSpeed(JUMP_STATIC_RIGHT_FINISH, glm::ivec2(0, 0), 2);

		sprite->setAnimationSpeed(JUMP_STATIC_LEFT_FINISH, 8);
		pos = sprite_pos[JUMP_STATIC_LEFT_FINISH] * SPRITE_Y;
		for (int i = 1; i >= 0; --i) sprite->addKeyframe(JUMP_STATIC_LEFT_FINISH, glm::vec2(i*SPRITE, pos));
		sprite->setSpeed(JUMP_STATIC_LEFT_FINISH, glm::ivec2(0, 0), 2);

		sprite->setAnimationSpeed(CLIMB_LEFT, 8);
		pos = sprite_pos[CLIMB_LEFT] * SPRITE_Y;
		for (int i = 11; i >= 0; --i) sprite->addKeyframe(CLIMB_LEFT, glm::vec2(i*SPRITE, pos));
		sprite->setSpeed(CLIMB_LEFT, glm::ivec2(0, 0), 5);
		sprite->setSpeed(CLIMB_LEFT, glm::ivec2(0, -1), 1);
		sprite->setSpeed(CLIMB_LEFT, glm::ivec2(-1, -1),1);
		sprite->setSpeed(CLIMB_LEFT, glm::ivec2(-1, -1), 1);
		sprite->setSpeed(CLIMB_LEFT, glm::ivec2(0, -1), 1);
		sprite->setSpeed(CLIMB_LEFT, glm::ivec2(0, -1), 1);
		sprite->setSpeed(CLIMB_LEFT, glm::ivec2(0, 0), 2);
				
		sprite->setAnimationSpeed(CLIMB_RIGHT, 8);
		pos = sprite_pos[CLIMB_RIGHT] * SPRITE_Y;
		for (int i = 0; i < 12; ++i) sprite->addKeyframe(CLIMB_RIGHT, glm::vec2(i*SPRITE, pos));
		sprite->setSpeed(CLIMB_RIGHT, glm::ivec2(0, 0), 5);
		sprite->setSpeed(CLIMB_RIGHT, glm::ivec2(2, -1), 1);
		sprite->setSpeed(CLIMB_RIGHT, glm::ivec2(1, -1), 1);
		sprite->setSpeed(CLIMB_RIGHT, glm::ivec2(1, -1), 1);
		sprite->setSpeed(CLIMB_RIGHT, glm::ivec2(0, -1), 1);
		sprite->setSpeed(CLIMB_RIGHT, glm::ivec2(0, -1),1);
		sprite->setSpeed(CLIMB_RIGHT, glm::ivec2(0, 0), 2);
		
		sprite->setAnimationSpeed(SQUAT_LEFT, 8);
		pos = sprite_pos[SQUAT_LEFT] * SPRITE_Y;
		for (int i = 2; i >= 0; --i) sprite->addKeyframe(SQUAT_LEFT, glm::vec2(i*SPRITE, pos));
		sprite->setSpeed(SQUAT_LEFT, glm::ivec2(0, 0), 3);

		sprite->setAnimationSpeed(SQUAT_RIGHT, 8);
		pos = sprite_pos[SQUAT_RIGHT] * SPRITE_Y;
		for (int i = 0; i < 3; ++i) sprite->addKeyframe(SQUAT_RIGHT, glm::vec2(i*SPRITE, pos));
		sprite->setSpeed(SQUAT_RIGHT, glm::ivec2(0, 0), 3);

		sprite->setAnimationSpeed(STANDUP_LEFT, 8);
		pos = sprite_pos[STANDUP_LEFT] * SPRITE_Y;
		for (int i = 6; i >= 0; --i) sprite->addKeyframe(STANDUP_LEFT, glm::vec2(i*SPRITE, pos));
		sprite->setSpeed(STANDUP_LEFT, glm::ivec2(0, 0), 7);
		
		sprite->setAnimationSpeed(STANDUP_RIGHT, 8);
		pos = sprite_pos[STANDUP_RIGHT] * SPRITE_Y;
		for (int i = 0; i < 7; ++i) sprite->addKeyframe(STANDUP_RIGHT, glm::vec2(i*SPRITE, pos));
		sprite->setSpeed(STANDUP_RIGHT, glm::ivec2(0, 0), 7);

		sprite->setAnimationSpeed(CROUCHED_LEFT, 8);
		pos = sprite_pos[CROUCHED_LEFT] * SPRITE_Y;
		sprite->addKeyframe(CROUCHED_LEFT, glm::vec2(0.0f, pos));
		sprite->setSpeed(CROUCHED_LEFT, glm::ivec2(0, 0), 1);

		sprite->setAnimationSpeed(CROUCHED_RIGHT, 8);
		pos = sprite_pos[CROUCHED_RIGHT] * SPRITE_Y;
		sprite->addKeyframe(CROUCHED_RIGHT, glm::vec2(2*SPRITE, pos));
		sprite->setSpeed(CROUCHED_RIGHT, glm::ivec2(0, 0), 1);

		sprite->setAnimationSpeed(JUMP_RUN_LEFT, 8);
		pos = sprite_pos[JUMP_RUN_LEFT] * SPRITE_Y;
		for (int i = 9; i >= 0; --i) sprite->addKeyframe(JUMP_RUN_LEFT, glm::vec2(i*SPRITE, pos));
		sprite->setSpeed(JUMP_RUN_LEFT, glm::ivec2(-2, 0), 3);
		sprite->setSpeed(JUMP_RUN_LEFT, glm::ivec2(-3, -1), 3);
		sprite->setSpeed(JUMP_RUN_LEFT, glm::ivec2(-2, 1), 3);
		sprite->setSpeed(JUMP_RUN_LEFT, glm::ivec2(-2, 0), 1);

		sprite->setAnimationSpeed(JUMP_RUN_RIGHT, 8);
		pos = sprite_pos[JUMP_RUN_RIGHT] * SPRITE_Y;
		for (int i = 0; i < 10; ++i) sprite->addKeyframe(JUMP_RUN_RIGHT, glm::vec2(i*SPRITE, pos));
		sprite->setSpeed(JUMP_RUN_RIGHT, glm::ivec2(2, 0), 3);
		sprite->setSpeed(JUMP_RUN_RIGHT, glm::ivec2(3, -1), 3);
		sprite->setSpeed(JUMP_RUN_RIGHT, glm::ivec2(2, 1), 3);
		sprite->setSpeed(JUMP_RUN_RIGHT, glm::ivec2(2, 0), 1);

		sprite->setAnimationSpeed(FALL_LEFT, 8);
		pos = sprite_pos[FALL_LEFT] * SPRITE_Y;
		sprite->addKeyframe(FALL_LEFT, glm::vec2(0.0, pos));
		sprite->setSpeed(FALL_LEFT, glm::ivec2(0, 0), 1);

		sprite->setAnimationSpeed(FALL_RIGHT, 8);
		pos = sprite_pos[FALL_RIGHT] * SPRITE_Y;
		sprite->addKeyframe(FALL_RIGHT, glm::vec2(0.0, pos));
		sprite->setSpeed(FALL_RIGHT, glm::ivec2(0, 0), 1);


		sprite->setAnimationSpeed(CROUCHED_FALL_LEFT, 1);
		pos = sprite_pos[CROUCHED_FALL_LEFT] * SPRITE_Y;
		sprite->addKeyframe(CROUCHED_FALL_LEFT, glm::vec2(0.0f, pos));
		sprite->addKeyframe(CROUCHED_FALL_LEFT, glm::vec2(0.0f, pos));
		sprite->setSpeed(CROUCHED_FALL_LEFT, glm::ivec2(0, 0), 2);

		sprite->setAnimationSpeed(CROUCHED_FALL_RIGHT, 1);
		pos = sprite_pos[CROUCHED_FALL_RIGHT] * SPRITE_Y;
		sprite->addKeyframe(CROUCHED_FALL_RIGHT, glm::vec2(2 * SPRITE, pos));
		sprite->addKeyframe(CROUCHED_FALL_RIGHT, glm::vec2(2 * SPRITE, pos));
		sprite->setSpeed(CROUCHED_FALL_RIGHT, glm::ivec2(0, 0), 2);

		sprite->setAnimationSpeed(DEATH_RIGHT, 8);
		sprite->setLoop(DEATH_RIGHT, false);
		pos = sprite_pos[DEATH_RIGHT] * SPRITE_Y;
		for (int i = 0; i < 5; ++i) sprite->addKeyframe(DEATH_RIGHT, glm::vec2(i*SPRITE, pos));
		sprite->setSpeed(DEATH_RIGHT, glm::ivec2(0, 0), 5);

		sprite->setAnimationSpeed(DEATH_LEFT, 8);
		sprite->setLoop(DEATH_LEFT, false);
		pos = sprite_pos[DEATH_LEFT] * SPRITE_Y;
		for (int i = 4; i >= 0; --i) sprite->addKeyframe(DEATH_LEFT, glm::vec2(i*SPRITE, pos));
		sprite->setSpeed(DEATH_LEFT, glm::ivec2(0, 0), 5);

		sprite->setAnimationSpeed(UNSHEATHE_RIGHT, 8);
		sprite->setLoop(UNSHEATHE_RIGHT, false);
		pos = sprite_pos[UNSHEATHE_RIGHT] * SPRITE_Y;
		for (int i = 0; i < 5; ++i) sprite->addKeyframe(UNSHEATHE_RIGHT, glm::vec2(i*SPRITE, pos));
		sprite->setSpeed(UNSHEATHE_RIGHT, glm::ivec2(0, 0), 5);

		sprite->setAnimationSpeed(UNSHEATHE_LEFT, 8);
		sprite->setLoop(UNSHEATHE_LEFT, false);
		pos = sprite_pos[UNSHEATHE_LEFT] * SPRITE_Y;
		for (int i = 4; i>=0; --i) sprite->addKeyframe(UNSHEATHE_LEFT, glm::vec2(i*SPRITE, pos));
		sprite->setSpeed(UNSHEATHE_LEFT, glm::ivec2(0, 0), 5);

		sprite->setAnimationSpeed(SHEATHE_RIGHT, 8);
		pos = sprite_pos[SHEATHE_RIGHT] * SPRITE_Y;
		for (int i = 0; i < 9; ++i) sprite->addKeyframe(SHEATHE_RIGHT, glm::vec2(i*SPRITE, pos));
		sprite->setSpeed(SHEATHE_RIGHT, glm::ivec2(0, 0), 9);

		sprite->setAnimationSpeed(SHEATHE_LEFT, 8);
		pos = sprite_pos[SHEATHE_LEFT] * SPRITE_Y;
		for (int i = 8; i >=0; --i) sprite->addKeyframe(SHEATHE_LEFT, glm::vec2(i*SPRITE, pos));
		sprite->setSpeed(SHEATHE_LEFT, glm::ivec2(0, 0), 9);

		sprite->setAnimationSpeed(ATTACK_RIGHT, 8);
		pos = sprite_pos[ATTACK_RIGHT] * SPRITE_Y;
		for (int i = 0; i <8; ++i) sprite->addKeyframe(ATTACK_RIGHT, glm::vec2(i*SPRITE, pos));
		sprite->setSpeed(ATTACK_RIGHT, glm::ivec2(0, 0), 4);
		sprite->setSpeed(ATTACK_RIGHT, glm::ivec2(1, 0), 1);
		sprite->setSpeed(ATTACK_RIGHT, glm::ivec2(0, 0), 1);
		sprite->setSpeed(ATTACK_RIGHT, glm::ivec2(0, 0), 2);

		sprite->setAnimationSpeed(ATTACK_LEFT, 8);
		pos = sprite_pos[ATTACK_LEFT] * SPRITE_Y;
		for (int i =7; i >=0; --i) sprite->addKeyframe(ATTACK_LEFT, glm::vec2(i*SPRITE, pos));
		sprite->setSpeed(ATTACK_LEFT, glm::ivec2(0, 0), 4);
		sprite->setSpeed(ATTACK_LEFT, glm::ivec2(-1, 0), 1);
		sprite->setSpeed(ATTACK_LEFT, glm::ivec2(0, 0), 1);
		sprite->setSpeed(ATTACK_LEFT, glm::ivec2(0, 0), 2);

		sprite->setAnimationSpeed(PROTECT_RIGHT, 8);
		pos = sprite_pos[PROTECT_RIGHT] * SPRITE_Y;
		for (int i = 0; i <4; ++i) sprite->addKeyframe(PROTECT_RIGHT, glm::vec2(i*SPRITE, pos));
		sprite->setSpeed(PROTECT_RIGHT, glm::ivec2(0, 0), 4);

		sprite->setAnimationSpeed(PROTECT_LEFT, 8);
		pos = sprite_pos[PROTECT_LEFT] * SPRITE_Y;
		for (int i = 3; i >= 0; --i) sprite->addKeyframe(PROTECT_LEFT, glm::vec2(i*SPRITE, pos));
		sprite->setSpeed(PROTECT_LEFT, glm::ivec2(0, 0), 4);

		sprite->setAnimationSpeed(RIPOSTE_RIGHT, 8);
		pos = sprite_pos[RIPOSTE_RIGHT] * SPRITE_Y;
		sprite->addKeyframe(RIPOSTE_RIGHT, glm::vec2(4*SPRITE, pos));
		sprite->setSpeed(RIPOSTE_RIGHT, glm::ivec2(0, 0), 1);

		sprite->setAnimationSpeed(RIPOSTE_LEFT, 8);
		pos = sprite_pos[RIPOSTE_LEFT] * SPRITE_Y;
		sprite->addKeyframe(RIPOSTE_LEFT, glm::vec2(0, pos));
		sprite->setSpeed(RIPOSTE_LEFT, glm::ivec2(0, 0), 1);

		sprite->setAnimationSpeed(DEATH_SPIKES_LEFT, 8);
		pos = sprite_pos[DEATH_SPIKES_LEFT] * SPRITE_Y;
		sprite->addKeyframe(DEATH_SPIKES_LEFT, glm::vec2(SPRITE, pos));
		sprite->setSpeed(DEATH_SPIKES_LEFT, glm::ivec2(0, 0), 1);

		sprite->setAnimationSpeed(DEATH_SPIKES_RIGHT, 8);
		pos = sprite_pos[DEATH_SPIKES_RIGHT] * SPRITE_Y;
		sprite->addKeyframe(DEATH_SPIKES_RIGHT, glm::vec2(SPRITE , pos));
		sprite->setSpeed(DEATH_SPIKES_RIGHT, glm::ivec2(0, 0), 1);

		sprite->setAnimationSpeed(POTION_LEFT, 8);
		pos = sprite_pos[POTION_LEFT] * SPRITE_Y;
		for (int i = 0; i < 11; ++i) sprite->addKeyframe(POTION_LEFT, glm::vec2(i*SPRITE, pos));
		sprite->setSpeed(POTION_LEFT, glm::ivec2(0, 0), 11);

		sprite->setAnimationSpeed(POTION_RIGHT, 8);
		pos = sprite_pos[POTION_RIGHT] * SPRITE_Y;
		for (int i = 10; i>= 0; --i) sprite->addKeyframe(POTION_RIGHT, glm::vec2(i*SPRITE, pos));
		sprite->setSpeed(POTION_RIGHT, glm::ivec2(0, 0), 11);

		sprite->setAnimationSpeed(DOOR_LEFT, 8);
		pos = sprite_pos[DOOR_LEFT] * SPRITE_Y;
		for (int i = 9; i >= 0; --i)sprite->addKeyframe(DOOR_LEFT, glm::vec2(i*SPRITE, pos));
		sprite->setSpeed(DOOR_LEFT, glm::ivec2(0, 0), 10);

		sprite->setAnimationSpeed(DOOR_RIGHT, 8);
		sprite->setLoop(DOOR_RIGHT, false);
		pos = sprite_pos[DOOR_RIGHT] * SPRITE_Y;
		for (int i = 0; i < 10; ++i) sprite->addKeyframe(DOOR_RIGHT, glm::vec2(i*SPRITE, pos));
		sprite->setSpeed(DOOR_RIGHT, glm::ivec2(0, 0), 10);

		sprite->setAnimationSpeed(DESCEND_LEFT, 8);
		pos = sprite_pos[DESCEND_LEFT] * SPRITE_Y;
		for (int i = 5; i <12; ++i) sprite->addKeyframe(DESCEND_LEFT, glm::vec2(i*SPRITE, pos));
		sprite->setSpeed(DESCEND_LEFT, glm::ivec2(0, 0), 12);

		sprite->setAnimationSpeed(DESCEND_RIGHT, 8);
		pos = sprite_pos[DESCEND_RIGHT] * SPRITE_Y;
		for (int i = 6; i >= 0; --i) sprite->addKeyframe(DESCEND_RIGHT, glm::vec2(i*SPRITE, pos));
		sprite->setSpeed(DESCEND_RIGHT, glm::ivec2(-1, 0), 1);
		sprite->setSpeed(DESCEND_RIGHT, glm::ivec2(0, 0), 1);
		sprite->setSpeed(DESCEND_RIGHT, glm::ivec2(0, 0), 1);
		sprite->setSpeed(DESCEND_RIGHT, glm::ivec2(0, 0), 2);

		sprite->setAnimationSpeed(CLIMB_WAIT_LEFT, 8);
		pos = sprite_pos[CLIMB_WAIT_LEFT] * SPRITE_Y;
		sprite->addKeyframe(CLIMB_WAIT_LEFT, glm::vec2(11 * SPRITE, pos));
		sprite->setSpeed(CLIMB_WAIT_LEFT, glm::ivec2(0, 0), 1);

		sprite->setAnimationSpeed(CLIMB_WAIT_RIGHT, 8);
		pos = sprite_pos[CLIMB_WAIT_RIGHT] * SPRITE_Y;
		sprite->addKeyframe(CLIMB_WAIT_RIGHT, glm::vec2(0.0, pos));
		sprite->setSpeed(CLIMB_WAIT_RIGHT, glm::ivec2(0, 0), 1);

		sprite->setAnimationSpeed(SNAKE_RIGHT, 8);
		pos = sprite_pos[SNAKE_RIGHT] * SPRITE_Y;
		for (int i = 2; i < 16; i++) sprite->addKeyframe(SNAKE_RIGHT, glm::vec2(i*SPRITE, pos));
		sprite->setSpeed(SNAKE_RIGHT, glm::ivec2(0, 0), 7);
		sprite->setSpeed(SNAKE_RIGHT, glm::ivec2(3, 0), 3);
		sprite->setSpeed(SNAKE_RIGHT, glm::ivec2(0, 0), 4);

		sprite->setAnimationSpeed(SNAKE_LEFT, 8);
		pos = sprite_pos[SNAKE_LEFT] * SPRITE_Y;
		for (int i = 15; i >= 2; i--) sprite->addKeyframe(SNAKE_LEFT, glm::vec2(i*SPRITE, pos));
		sprite->setSpeed(SNAKE_LEFT, glm::ivec2(0, 0), 7);
		sprite->setSpeed(SNAKE_LEFT, glm::ivec2(-3, 0), 3);
		sprite->setSpeed(SNAKE_LEFT, glm::ivec2(0, 0), 4);


	leftMovement = rightMovement = moveXclimb = center = colisionRight = colisionLeft = colisionUp = colisionDown = canvi = sound = false;
	climb = 0;
	fallDepth = 1;
	lives = 3;
	maxLife = 3;
	sprite->changeAnimation(1);
	tileMapDispl = tileMapPos;
	//sprite->setPosition(glm::vec2(float(tileMapDispl.x + posPlayer.x), float(tileMapDispl.y + posPlayer.y)));
	sprite->setPosition(tileMapDispl, posPlayer);
	
}

void Player::update(int deltaTime)
{

	sprite->update(deltaTime);
	if(!bJumping) posPlayer.y += FALL_STEP;
	/*printf("POS PLAYER  : %d  %d TILE  PLAYER  : %d  %d  TILE TIPUS : %d DRE : %d  ESQ %d  \n", posPlayer.x, posPlayer.y, get_player_tile().x, get_player_tile().y,
		(map->getTileFromTile(glm::ivec2(get_player_tile().x - 1, get_player_tile().y))), (map->getTileFromTile(glm::ivec2(get_player_tile().x, get_player_tile().y))), 
		(map->getTileFromTile(glm::ivec2(get_player_tile().x - 2, get_player_tile().y))));*/
	colisionRight = map->collisionMoveRight(glm::ivec2(posPlayer.x, posPlayer.y), glm::ivec2(32, 64));
	colisionLeft = map->collisionMoveLeft(posPlayer, glm::ivec2(32, 64));
	colisionUp = map->collisionMoveUp(glm::ivec2(posPlayer.x, posPlayer.y+4), glm::ivec2(32, 64), &climb, center);
	colisionDown = map->collisionMoveDown(glm::ivec2(posPlayer.x-5, posPlayer.y), glm::ivec2(32, 64), &posPlayer.y);

	bJumping = canvi_lvl = false;
		if (Game::instance().getKey(108)|| Game::instance().getKey(76)){
			Game::instance().nextLevel();

		}
	switch (sprite->animation()){
	case STAND_LEFT:
		if (colisionDown){
			if (Game::instance().getSpecialKey(GLUT_KEY_RIGHT)){
				sprite->changeAnimation(CHANGE_TO_RIGHT);
			}
			else if (Game::instance().getSpecialKey(GLUT_KEY_LEFT) && Game::instance().getSpecialKey(112)){
				sprite->changeAnimation(MOVE_LEFT_SLOW);
			}
			else if (Game::instance().getSpecialKey(GLUT_KEY_LEFT)){
				
				sprite->changeAnimation(MOVE_LEFT);
			}
			else if (Game::instance().getSpecialKey(GLUT_KEY_UP)){
				if (climb == 1){
					posPlayer.x -= posPlayer.x % 32 - 10;
					//posPlayer.y += 4;
					sprite->changeAnimation(JUMP_STATIC_LEFT);
				}
				else if (map->checkFinalDoor(posPlayer, &tileDoorX)){
					posPlayer.y -= 10;
					posPlayer.x = tileDoorX * 32;
					sprite->changeAnimation(DOOR_RIGHT);
				}
				else{
					sprite->changeAnimation(JUMP_STATIC_LEFT);
				}
			}
			else if (Game::instance().getSpecialKey(GLUT_KEY_DOWN)){
				sprite->changeAnimation(SQUAT_LEFT);
			}
			else if (hasSword && (Game::instance().getKey(115) || Game::instance().getKey(83))){
				unsheathe = true;
				sprite->changeAnimation(UNSHEATHE_LEFT);
			}
			else if (Game::instance().getKey(79) || Game::instance().getKey(111)){
				sprite->changeAnimation(SNAKE_LEFT);
			}
		}
		else{
			fallDepth =7;
			sprite->changeAnimation(FALL_LEFT);
		}
		break;

	case STAND_RIGHT:
		if (colisionDown){
			if (Game::instance().getSpecialKey(GLUT_KEY_LEFT)){
				sprite->changeAnimation(CHANGE_TO_LEFT);
			}
			else if (Game::instance().getSpecialKey(GLUT_KEY_RIGHT) && Game::instance().getSpecialKey(112)){
				sprite->changeAnimation(MOVE_RIGHT_SLOW);
			}
			else if (Game::instance().getSpecialKey(GLUT_KEY_RIGHT)){
				sprite->changeAnimation(MOVE_RIGHT);
			}
			else if (Game::instance().getSpecialKey(GLUT_KEY_UP)){
			
				if (climb == 2){
					posPlayer.x -= (posPlayer.x % 32) + 5;
					sprite->changeAnimation(JUMP_STATIC_RIGHT);
				}
				else if (center){
					posPlayer.x -= (posPlayer.x % 32) + 5;
					posPlayer.x += 32;
					sprite->changeAnimation(JUMP_STATIC_RIGHT);

				}
				else if (map->checkFinalDoor(posPlayer, &tileDoorX)){
					posPlayer.y -= 10;
					posPlayer.x = tileDoorX * 32;
					sprite->changeAnimation(DOOR_RIGHT);
				}
				else{
					sprite->changeAnimation(JUMP_STATIC_RIGHT);
				}
			}
			else if (Game::instance().getSpecialKey(GLUT_KEY_DOWN)){
					sprite->changeAnimation(SQUAT_RIGHT);
			}
			else if (hasSword && Game::instance().getKey(115) || Game::instance().getKey(83)){
				unsheathe = true;
				sprite->changeAnimation(UNSHEATHE_RIGHT);
			}
			else if (Game::instance().getKey(79) || Game::instance().getKey(111)){
				sprite->changeAnimation(SNAKE_RIGHT);
			}
		}
		else{
			fallDepth = 7;
			sprite->changeAnimation(FALL_RIGHT);
		}
		break;

	case MOVE_LEFT:
		if (colisionLeft){
			PlaySound(TEXT("sounds/colision.wav"), NULL, SND_FILENAME | SND_ASYNC);
			posPlayer.x += 2;
			sprite->changeAnimation(STAND_LEFT);
		}
		else if (!colisionDown){
			posPlayer.x -= 8;
			fallDepth = 1;
			sprite->changeAnimation(FALL_LEFT);
		}
		else{

			if (sprite->isFinished()){
				if (Game::instance().getSpecialKey(GLUT_KEY_LEFT)){
					sprite->changeAnimation(RUN_LEFT);
				}
				else {
					PlaySound(TEXT("sounds/stop.wav"), NULL, SND_FILENAME | SND_ASYNC);
					sprite->changeAnimation(STOP_LEFT);
				}
			}
			else if (Game::instance().getSpecialKey(GLUT_KEY_UP)){
				sprite->changeAnimation(JUMP_STAND_LEFT);
			}
		}
		break;

	case MOVE_RIGHT:
		if (colisionRight){
			posPlayer.x -= 2;
			PlaySound(TEXT("sounds/colision.wav"), NULL, SND_FILENAME | SND_ASYNC);
			sprite->changeAnimation(STAND_RIGHT);
		}
		else if (!colisionDown){
			posPlayer.x += 4;
			fallDepth = 1;
			sprite->changeAnimation(FALL_RIGHT);
		}
		else{
			if (sprite->isFinished()){
				if (Game::instance().getSpecialKey(GLUT_KEY_RIGHT)){
					sprite->changeAnimation(RUN_RIGHT);
				}
				else{
					PlaySound(TEXT("sounds/stop.wav"), NULL, SND_FILENAME | SND_ASYNC);
					sprite->changeAnimation(STOP_RIGHT);
				}
			}
			else if (Game::instance().getSpecialKey(GLUT_KEY_UP)){
				sprite->changeAnimation(JUMP_STAND_RIGHT);
			}
		}
		break;

	case MOVE_LEFT_SLOW:
		if (colisionLeft){
			PlaySound(TEXT("sounds/colision.wav"), NULL, SND_FILENAME | SND_ASYNC);
			posPlayer.x += 2;
			sprite->changeAnimation(STAND_LEFT);
		}
		else if (!colisionDown){
			posPlayer.x -= 8;
			fallDepth = 1;
			sprite->changeAnimation(FALL_LEFT);
		}
		else{
			if (sprite->isFinished()){
				sprite->changeAnimation(STAND_LEFT);
			}
		}
		break;

	case MOVE_RIGHT_SLOW:
		if (colisionRight){
			PlaySound(TEXT("sounds/colision.wav"), NULL, SND_FILENAME | SND_ASYNC);
			posPlayer.x -= 2;
			sprite->changeAnimation(STAND_RIGHT);
		}
		else if (!colisionDown){
			posPlayer.x += 4;
			fallDepth = 1;
			sprite->changeAnimation(FALL_RIGHT);
		}
		else{
			if (sprite->isFinished()){
				sprite->changeAnimation(STAND_RIGHT);
			}
		}
		break;
	case STOP_LEFT:
		if (colisionLeft){
			PlaySound(TEXT("sounds/colision.wav"), NULL, SND_FILENAME | SND_ASYNC);
			posPlayer.x += 2;
			sprite->changeAnimation(STAND_LEFT);
		}
		else if (!colisionDown){
			posPlayer.x -= 8;
			fallDepth = 1;
			sprite->changeAnimation(FALL_LEFT);
		}
		else{
			if (sprite->isFinished()){
				sprite->changeAnimation(STAND_LEFT);
			}
		}
		break;

	case STOP_RIGHT:
		if (colisionRight){
			PlaySound(TEXT("sounds/colision.wav"), NULL, SND_FILENAME | SND_ASYNC);
			posPlayer.x -= 2;
			sprite->changeAnimation(STAND_RIGHT);
		}
		if (!colisionDown){
			posPlayer.y += 4;
			fallDepth = 1;
			sprite->changeAnimation(FALL_RIGHT);
		}
		else{
			if (sprite->isFinished()){
				sprite->changeAnimation(STAND_RIGHT);
			}
		}
		break;

	case CHANGE_TO_LEFT:
		if (sprite->isFinished()){
			sprite->changeAnimation(STAND_LEFT);
		}
		break;

	case CHANGE_TO_RIGHT:
		if (sprite->isFinished()){
			sprite->changeAnimation(STAND_RIGHT);
		}
		break;

	case RUN_LEFT:
		if (colisionLeft){
			PlaySound(TEXT("sounds/colision.wav"), NULL, SND_FILENAME | SND_ASYNC);
			posPlayer.x += 2;
			sprite->changeAnimation(STAND_LEFT);
		}
		else if (!colisionDown){
			fallDepth = 1;
			posPlayer.x -= 8;
			sprite->changeAnimation(FALL_LEFT);
		}
		else if (map->getTile(posPlayer) == 16){
			PlaySound(TEXT("sounds/regular_death.wav"), NULL, SND_FILENAME | SND_ASYNC);
			lives = 0;
			sprite->changeAnimation(DEATH_SPIKES_LEFT);
		}
		else{
			if ((sprite->getCurrentFrame() == 1 || sprite->getCurrentFrame() == 4)){
				if(!sound) PlaySound(TEXT("sounds/step.wav"), NULL, SND_FILENAME | SND_ASYNC);
				sound = true;
			}
			else{
				sound = false;
			}
			if (Game::instance().getSpecialKey(GLUT_KEY_RIGHT)){
				sprite->changeAnimation(RUN_CHANGE_TO_RIGHT);
			}
			else if (!Game::instance().getSpecialKey(GLUT_KEY_LEFT)){
				PlaySound(TEXT("sounds/stop.wav"), NULL, SND_FILENAME | SND_ASYNC);
				sprite->changeAnimation(STOP_LEFT);
			}
			else if (Game::instance().getSpecialKey(GLUT_KEY_UP)){
				sprite->changeAnimation(JUMP_RUN_LEFT);
			}
		}
		break;

	case RUN_RIGHT:
		if (colisionRight){
			PlaySound(TEXT("sounds/colision.wav"), NULL, SND_FILENAME | SND_ASYNC);
			posPlayer.x -= 2;
			sprite->changeAnimation(STAND_RIGHT);
		}
		else if (!colisionDown){
			fallDepth = 1;
			posPlayer.x += 5;
			sprite->changeAnimation(FALL_RIGHT);
		}
		else if (map->getTile(posPlayer) == 16){
			PlaySound(TEXT("sounds/regular_death.wav"), NULL, SND_FILENAME | SND_ASYNC);
			lives = 0;
			sprite->changeAnimation(DEATH_SPIKES_RIGHT);
		}
		else{
			if ((sprite->getCurrentFrame() == 1 || sprite->getCurrentFrame() == 4)){
				if (!sound) PlaySound(TEXT("sounds/step.wav"), NULL, SND_FILENAME | SND_ASYNC);
				sound = true;
			}
			else{
				sound = false;
			}
			if (Game::instance().getSpecialKey(GLUT_KEY_LEFT)){
				sprite->changeAnimation(RUN_CHANGE_TO_LEFT);
			}
			else if (!Game::instance().getSpecialKey(GLUT_KEY_RIGHT)){
				PlaySound(TEXT("sounds/stop.wav"), NULL, SND_FILENAME | SND_ASYNC);
				sprite->changeAnimation(STOP_RIGHT);
			}
			else if (Game::instance().getSpecialKey(GLUT_KEY_UP)){
				sprite->changeAnimation(JUMP_RUN_RIGHT);
			}
		}
		break;

	case RUN_CHANGE_TO_LEFT:
		if (colisionRight){
			PlaySound(TEXT("sounds/colision.wav"), NULL, SND_FILENAME | SND_ASYNC);
			posPlayer.x -= 2;
		}
		else if (!colisionDown){
			fallDepth = 1;
			sprite->changeAnimation(FALL_LEFT);
		}
		else{
			if (sprite->isFinished()){
				sprite->changeAnimation(RUN_LEFT);
			}
		}
		break;

	case RUN_CHANGE_TO_RIGHT:
		if (colisionLeft){
			PlaySound(TEXT("sounds/colision.wav"), NULL, SND_FILENAME | SND_ASYNC);
			posPlayer.x += 2;
		}
		else if (!colisionDown){
			fallDepth = 1;
			sprite->changeAnimation(FALL_RIGHT);
		}
		else{
			if (sprite->isFinished()){
				sprite->changeAnimation(RUN_RIGHT);
			}
		}
		break;

	case SQUAT_LEFT:
		if (sprite->isFinished()){
			if ((!hasSword) & (posPlayer.x - (posPlayer.x % 32) == 64) && (posPlayer.y - (posPlayer.y % 64) == 256)){
				sprite->changeAnimation(SHEATHE_LEFT);
				PlaySound(TEXT("sounds/guard_death_and_obtaining_the_sword.wav"), NULL, SND_FILENAME | SND_ASYNC);
				hasSword = true;
			}
			else{
				sprite->changeAnimation(CROUCHED_LEFT);
			}
		}
		break;

	case SQUAT_RIGHT:
		if (sprite->isFinished()){
			if ((!hasSword) & (posPlayer.x - (posPlayer.x % 32) == 64) && (posPlayer.y - (posPlayer.y % 64) == 256)){
				sprite->changeAnimation(SHEATHE_RIGHT);
				PlaySound(TEXT("sounds/guard_death_and_obtaining_the_sword.wav"), NULL, SND_FILENAME | SND_ASYNC);
				hasSword = true;
			}
			else{
				sprite->changeAnimation(CROUCHED_RIGHT);

			}
		}
		break;

	case STANDUP_LEFT:
		if (sprite->isFinished()){
			sprite->changeAnimation(STAND_LEFT);
		}
		break;

	case STANDUP_RIGHT:
		if (sprite->isFinished()){
			sprite->changeAnimation(STAND_RIGHT);
		}
		break;

	case CROUCHED_LEFT:
		if (colisionDown){
			if (Game::instance().getSpecialKey(GLUT_KEY_DOWN)){
				sprite->changeAnimation(CROUCHED_LEFT);//PER PODER FER STOP CORRECTAMENT, LANIMACIO DE PER SI FA BUCLE
				if (Game::instance().getSpecialKey(112) && (map->getJumpTile(get_player_tile().x, get_player_tile().y) == 2)){
					if (map->getTileFromTile(glm::ivec2(get_player_tile().x - 2, get_player_tile().y)) != 9){
						posPlayer.y += 32;
						posPlayer.x = posPlayer.x - (posPlayer.x % 32) + 4;
						sprite->changeAnimation(DESCEND_RIGHT);
					}
				}
				else if (map->checkPotions(posPlayer) != -1){
					sprite->changeAnimation(POTION_LEFT);
				}
			}

			else{
				sprite->changeAnimation(STANDUP_LEFT);
			}
		}
		else{
			fallDepth = 1;
			sprite->changeAnimation(FALL_LEFT);
		}
		break;

	case CROUCHED_RIGHT:
		if (colisionDown){
			if (Game::instance().getSpecialKey(GLUT_KEY_DOWN)){
				sprite->changeAnimation(CROUCHED_RIGHT);//PER PODER FER STOP CORRECTAMENT, LANIMACIO DE PER SI FA BUCLE
				if (Game::instance().getSpecialKey(112) && (map->getJumpTile(get_player_tile().x, get_player_tile().y) == 1)){
					if (map->getTileFromTile(glm::ivec2(get_player_tile().x, get_player_tile().y)) != 9){
						posPlayer.y += 32;
						posPlayer.x = posPlayer.x - (posPlayer.x % 32) + 32 + 10;
						sprite->changeAnimation(DESCEND_LEFT);
					}
				}
				else if (map->checkPotions(posPlayer) != -1){
					sprite->changeAnimation(POTION_RIGHT);
				}
			}
			else{
				sprite->changeAnimation(STANDUP_RIGHT);
			}
		}
		else{
			fallDepth = 1;
			sprite->changeAnimation(FALL_RIGHT);
		}
		break;



	case JUMP_STATIC_LEFT:
		bJumping = true;
		if (sprite->isFinished()){
			if (colisionDown){
				sprite->changeAnimation(JUMP_STATIC_LEFT_FINISH);
			}
			else if (colisionUp){
				posPlayer.y += 2;
				sprite->changeAnimation(STAND_LEFT);
			}
			else if (climb == 1){
				posPlayer.x -= posPlayer.x % 32 - 10;
				sprite->changeAnimation(CLIMB_WAIT_LEFT);
			}
			else bJumping = false;
		}
		else if (colisionUp){
			posPlayer.y += 2;
			sprite->changeAnimation(STAND_LEFT);
		}
		break;

	case JUMP_STATIC_RIGHT:
		bJumping = true;
		if (sprite->isFinished()){
		//	printf("%d   %d  POS PLAYER \n", posPlayer.x, posPlayer.y);
			if (colisionDown){
				sprite->changeAnimation(JUMP_STATIC_RIGHT_FINISH);
			}
			else if (colisionUp){
				posPlayer.y += 2;
				sprite->changeAnimation(STAND_RIGHT);
			}
			else if (climb == 2){
				sprite->changeAnimation(CLIMB_WAIT_RIGHT);
				posPlayer.y += 2;

			}
			else if (center){
				posPlayer.y += 2;
				sprite->changeAnimation(CLIMB_WAIT_RIGHT);
			}
			else bJumping = false;
		}
		else if (colisionUp){
			posPlayer.y += 2;
			sprite->changeAnimation(STAND_RIGHT);
		}
		break;

	case JUMP_STATIC_LEFT_FINISH:
		if (sprite->isFinished()){
			sprite->changeAnimation(STAND_LEFT);
		}

		break;
	case JUMP_STATIC_RIGHT_FINISH:
		if (sprite->isFinished()){
			sprite->changeAnimation(STAND_RIGHT);
		}

		break;

	case JUMP_STAND_LEFT:
		bJumping = true;
		if (colisionLeft){
			PlaySound(TEXT("sounds/colision.wav"), NULL, SND_FILENAME | SND_ASYNC);
			posPlayer.x += 10;
			if (!colisionDown){
				fallDepth = 1;
				sprite->changeAnimation(FALL_LEFT);
			}
			else{
				sprite->changeAnimation(STAND_LEFT);
			}
		}
		else if(sprite->getCurrentFrame()>8){
			if (!colisionDown){
				fallDepth = 1;
				sprite->changeAnimation(FALL_LEFT);
			}
			else if (sprite->isFinished()){
				sprite->changeAnimation(STAND_LEFT);
			}
		}
		break;

	case JUMP_STAND_RIGHT:
		bJumping = true;
		if (colisionRight){
			PlaySound(TEXT("sounds/colision.wav"), NULL, SND_FILENAME | SND_ASYNC);
			posPlayer.x -= 10;
			if (!colisionDown){
				fallDepth = 1;
				sprite->changeAnimation(FALL_RIGHT);
			}
			else{
				sprite->changeAnimation(STAND_RIGHT);
			}
		}
		else if (sprite->getCurrentFrame()>8){
			if (!colisionDown){
				fallDepth = 1;
				sprite->changeAnimation(FALL_RIGHT);
			}
			else if (sprite->isFinished()){
				sprite->changeAnimation(STAND_RIGHT);
			}
		}
		break;

	case CLIMB_LEFT:
		bJumping = true;

		if (sprite->isFinished()){
			sprite->changeAnimation(STAND_LEFT);
		}
		else if (sprite->getCurrentFrame() > 9 && sprite->getCurrentFrame() <11){
			posPlayer.y -= 2;
		}
		else if (sprite->getCurrentFrame() >5){
			if ((posPlayer.y + 64) % 64 < 10){
				posPlayer.y += 10 - (posPlayer.y % 64);
			}
		}
		break;
	case CLIMB_RIGHT:
		bJumping = true;
		//printf("POS PLAYER Y :%d \n", posPlayer.y);
		if (sprite->isFinished()){
			//printf("POS PLAYER Y :%d \n", posPlayer.y);
			//printf("POS PLAYER Y :%d \n", posPlayer.y);
			posPlayer.y -= 5;
			sprite->changeAnimation(STAND_RIGHT);
		}
		else if (sprite->getCurrentFrame() > 9 && sprite->getCurrentFrame() <11){
			posPlayer.y -=2;
		}
		else if (sprite->getCurrentFrame() >5){
			if ((posPlayer.y + 64) % 64 < 10){
				posPlayer.y += 10 - (posPlayer.y % 64);
			}
		}
		break;
	case JUMP_RUN_LEFT:
		bJumping = true;
		if (colisionLeft){
			PlaySound(TEXT("sounds/colision.wav"), NULL, SND_FILENAME | SND_ASYNC);
			posPlayer.x += 10;
			if (!colisionDown){
				fallDepth = 1;
				sprite->changeAnimation(FALL_LEFT);
			}
			else{
				sprite->changeAnimation(STAND_LEFT);
			}
			bJumping = false;
		}
		else{
			if (sprite->getCurrentFrame() >7){
				bJumping = false;
				if (!colisionDown){
					fallDepth = 1;
					sprite->changeAnimation(FALL_LEFT);
				}
				else if (sprite->isFinished()){
					sprite->changeAnimation(STAND_LEFT);
				}
			}
		}
		break;
	case JUMP_RUN_RIGHT:
		bJumping = true;
		if (colisionRight){
			PlaySound(TEXT("sounds/colision.wav"), NULL, SND_FILENAME | SND_ASYNC);
			posPlayer.x -= 10;
			if (!colisionDown){
				fallDepth = 1;
				sprite->changeAnimation(FALL_RIGHT);
			}
			else{
				sprite->changeAnimation(STAND_RIGHT);
			}
			bJumping = false;
		}
		else{
			if (sprite->isFinished()){
				bJumping = false;
				if (!colisionDown){
					fallDepth = 1;
					sprite->changeAnimation(FALL_RIGHT);
				}
				else{
					sprite->changeAnimation(STAND_RIGHT);
				}
			}
		}
		break;
	
	case FALL_LEFT:
		fallDepth++;
		if (colisionDown){
			haEscalat = false;
			//printf("FALLDEPTH ESQUERRA %d \n", fallDepth);
			if (map->getTile(posPlayer) == 16){
				lives = 0;
				PlaySound(TEXT("sounds/regular_death.wav"), NULL, SND_FILENAME | SND_ASYNC);
				sprite->changeAnimation(DEATH_SPIKES_LEFT);
			}
			else if (fallDepth > 20 && fallDepth < 35){
				PlaySound(TEXT("sounds/fall2.wav"), NULL, SND_FILENAME | SND_ASYNC);
				sprite->changeAnimation(SQUAT_LEFT);
			}
			else if (fallDepth > 30 && fallDepth < 60){
				//printf("CROUCHED LEFT FALL\n");
				sprite->changeAnimation(CROUCHED_FALL_LEFT);
				lives--;
				if (lives == 0){
					PlaySound(TEXT("sounds/regular_death.wav"), NULL, SND_FILENAME | SND_ASYNC);
					sprite->changeAnimation(DEATH_LEFT);
				}
				else{
					PlaySound(TEXT("sounds/fall1.wav"), NULL, SND_FILENAME | SND_ASYNC);
				}
			}
			else if (fallDepth > 60){
				lives = 0;
				PlaySound(TEXT("sounds/regular_death.wav"), NULL, SND_FILENAME | SND_ASYNC);
				sprite->changeAnimation(DEATH_LEFT);
			}
			else{
				sprite->changeAnimation(STAND_LEFT);
			}
		}
		else if (colisionLeft){
			posPlayer.x += 10;
		}
		else if (!haEscalat && Game::instance().getSpecialKey(112) && (map->getJumpTile(get_player_tile().x-1, get_player_tile().y-1) == 1)){
				posPlayer.x -= posPlayer.x % 32 - 10;
				posPlayer.y -= 20;
				haEscalat = true;
				sprite->changeAnimation(CLIMB_WAIT_LEFT);
		}
			
		break;

	case FALL_RIGHT:
		fallDepth++;
		if (colisionDown){
			haEscalat = false;
			//printf("FALLDEPTH %d \n", fallDepth);
			if (map->getTile(glm::ivec2(posPlayer.x +10, posPlayer.y)) == 16){
				posPlayer.x += 10;
				bJumping = true;
				lives = 0;
				PlaySound(TEXT("sounds/regular_death.wav"), NULL, SND_FILENAME | SND_ASYNC);
				sprite->changeAnimation(DEATH_SPIKES_RIGHT);
			}
			else if (fallDepth > 20 && fallDepth < 40){
				sprite->changeAnimation(SQUAT_RIGHT);
				PlaySound(TEXT("sounds/fall2.wav"), NULL, SND_FILENAME | SND_ASYNC);
			}
			else if (fallDepth > 40 && fallDepth < 60){
				sprite->changeAnimation(CROUCHED_FALL_RIGHT);
				lives--;
				if (lives == 0){
					PlaySound(TEXT("sounds/regular_death.wav"), NULL, SND_FILENAME | SND_ASYNC);
					sprite->changeAnimation(DEATH_RIGHT);
				}
				else{
					PlaySound(TEXT("sounds/fall1.wav"), NULL, SND_FILENAME | SND_ASYNC);
				}
			}
			else if (fallDepth > 60){
				lives = 0;
				PlaySound(TEXT("sounds/regular_death.wav"), NULL, SND_FILENAME | SND_ASYNC);
				sprite->changeAnimation(DEATH_RIGHT);   //FER ANIMACIO DE MORT
			}
			else{
				sprite->changeAnimation(STAND_RIGHT);
			}
		}
		else if (!haEscalat && Game::instance().getSpecialKey(112) && (map->getJumpTile(get_player_tile().x +1, get_player_tile().y - 1) == 2)){
			posPlayer.x -= posPlayer.x % 32 - 10;
			posPlayer.x += 20;
			posPlayer.y -= 20;
			haEscalat = true;
			sprite->changeAnimation(CLIMB_WAIT_RIGHT);
		}
		break;

	case CROUCHED_FALL_LEFT:
		if (sprite->isFinished()){
			sprite->changeAnimation(STANDUP_LEFT);
		}
		break;

	case CROUCHED_FALL_RIGHT:
		if (sprite->isFinished()){
			sprite->changeAnimation(STANDUP_RIGHT);
		}
		break;
		
	case UNSHEATHE_LEFT:
		if (sprite->isFinished()){
			sprite->changeAnimation(RIPOSTE_LEFT);
		}
		break;

	case UNSHEATHE_RIGHT:
		if (sprite->isFinished()){
			sprite->changeAnimation(RIPOSTE_RIGHT);
		}
		break;

	case RIPOSTE_LEFT:
		if (Game::instance().getSpecialKey(GLUT_KEY_LEFT)){
			sprite->changeAnimation(ATTACK_LEFT);
		}
		else if (Game::instance().getSpecialKey(GLUT_KEY_RIGHT)){
			sprite->changeAnimation(PROTECT_LEFT);
		}
		else if (Game::instance().getKey(115) || Game::instance().getKey(83)){
			sprite->changeAnimation(SHEATHE_LEFT);
		}

		break;

	case RIPOSTE_RIGHT:
		if (Game::instance().getSpecialKey(GLUT_KEY_RIGHT)){
			sprite->changeAnimation(ATTACK_RIGHT);
		}
		else if (Game::instance().getSpecialKey(GLUT_KEY_LEFT)){
			sprite->changeAnimation(PROTECT_RIGHT);
		}
		else if (Game::instance().getKey(115) || Game::instance().getKey(83)){
			sprite->changeAnimation(SHEATHE_RIGHT);
		}
		break;


	case SHEATHE_LEFT:
		if (sprite->isFinished()){
			unsheathe = false;
			sprite->changeAnimation(STAND_LEFT);
		}
		break;

	case SHEATHE_RIGHT:
		if (sprite->isFinished()){
			unsheathe = false;
			sprite->changeAnimation(STAND_RIGHT);
		}
		break;

	case ATTACK_LEFT:
		if (sprite->isFinished()){
			sprite->changeAnimation(RIPOSTE_LEFT);
		}
		else if (colisionLeft){
			PlaySound(TEXT("sounds/colision.wav"), NULL, SND_FILENAME | SND_ASYNC);
			sprite->changeAnimation(SHEATHE_LEFT);
		}
		break;

	case ATTACK_RIGHT:
		if (sprite->isFinished()){
			sprite->changeAnimation(RIPOSTE_RIGHT);
		}
		else if (colisionRight){
			PlaySound(TEXT("sounds/colision.wav"), NULL, SND_FILENAME | SND_ASYNC);
			sprite->changeAnimation(SHEATHE_RIGHT);
		}
		break;

	case PROTECT_LEFT:
		if (sprite->isFinished()){
			sprite->changeAnimation(RIPOSTE_LEFT);
		}
		break;

	case PROTECT_RIGHT:
		if (sprite->isFinished()){
			sprite->changeAnimation(RIPOSTE_RIGHT);
		}
		break;

	case DEATH_SPIKES_LEFT:
		break;

	case DEATH_SPIKES_RIGHT:

		break;

	case DOOR_RIGHT:
		bJumping = true;
		if (sprite->isFinished()){
			if (!canvi){
				canvi_lvl = true;
				canvi = true;
			}


			//Game::instance().levelPlus();
			//map->changeLevel();
		}

		break;

	case DESCEND_LEFT:
		bJumping = true;

		if (sprite->isFinished()){
			sprite->changeAnimation(CLIMB_WAIT_LEFT);
		}
		break;

	case DESCEND_RIGHT:
		bJumping = true;

		if (sprite->isFinished()){
			sprite->changeAnimation(CLIMB_WAIT_RIGHT);
		}
		break;

	case CLIMB_WAIT_LEFT:
		bJumping = true;
		if (Game::instance().getSpecialKey(GLUT_KEY_DOWN)){
			fallDepth = 1;
			posPlayer.x += 8;
			sprite->changeAnimation(FALL_LEFT);
		}
		else if (Game::instance().getSpecialKey(GLUT_KEY_UP)){
			posPlayer.y -= 4;
			sprite->changeAnimation(CLIMB_LEFT);
		}

		break;

	case CLIMB_WAIT_RIGHT:
		bJumping = true;
		if (Game::instance().getSpecialKey(GLUT_KEY_DOWN)){
			fallDepth = 1;
			sprite->changeAnimation(FALL_RIGHT);
		}
		else if (Game::instance().getSpecialKey(GLUT_KEY_UP)){
			sprite->changeAnimation(CLIMB_RIGHT);
		}

		break;

	case SNAKE_LEFT:
		if (sprite->isFinished()){
			sprite->changeAnimation(STAND_LEFT);
		}
		else if (colisionLeft){
			if (map->getTileFromTile(glm::ivec2(get_player_tile().x - 2, get_player_tile().y)) == 1 || map->getTileFromTile(glm::ivec2(get_player_tile().x - 1, get_player_tile().y)) == 1){
				posPlayer.x -= 3;
			}
			posPlayer.x += 3;
		}
		else if (!colisionDown){
			fallDepth = 1;
			sprite->changeAnimation(FALL_LEFT);
		}
		break;
	case SNAKE_RIGHT:
		if (sprite->isFinished()){
			sprite->changeAnimation(STAND_RIGHT);
		}
		else if (colisionLeft){
			if (map->getTileFromTile(glm::ivec2(get_player_tile().x , get_player_tile().y)) == 1 || map->getTileFromTile(glm::ivec2(get_player_tile().x - 1, get_player_tile().y)) == 1
				|| map->getTileFromTile(glm::ivec2(get_player_tile().x - 2, get_player_tile().y)) == 1){
				posPlayer.x +=20;
			}
			posPlayer.x -= 20;
		}
		else if (!colisionDown){
			fallDepth = 1;
			sprite->changeAnimation(FALL_RIGHT);
		}
		break;

	case POTION_LEFT:
		if (sprite->isFinished()){
			if (lives < maxLife)lives++;
			sprite->changeAnimation(STAND_LEFT);
		}
		break;

	case POTION_RIGHT:
		if (sprite->isFinished()){
			if (lives < maxLife)lives++;
			sprite->changeAnimation(STAND_RIGHT);
		}
		break;




	}
	//printf("CURRENT FRAME : %d    ANIMACIO: %d\n", sprite->getCurrentFrame(), sprite->animation());

		sprite->setPosition(tileMapDispl, posPlayer);
		map->checkScreen(posPlayer);
		map->checkDoors(posPlayer);
		map->checkGround(posPlayer, sprite->animation());

		if (canvi_lvl){
			//printf("HE ENTRAT CANVI LVL \n");
			Game::instance().levelPlus();
			canvi_lvl = false;
		}

}

void Player::render()
{
	sprite->render();
}

void Player::setTileMap(TileMap *tileMap)
{
	map = tileMap;
}

void Player::setPosition(const glm::vec2 &pos)
{
	posPlayer = pos;
	//sprite->setPosition(glm::vec2(float(tileMapDispl.x + posPlayer.x), float(tileMapDispl.y + posPlayer.y)));
	sprite->setPosition(tileMapDispl, posPlayer);
}

bool Player::death(){
	return(lives == 0);
}

int Player::get_lives(){
	return lives;
}

int Player::get_max_lives(){
	return maxLife;
}

bool Player::player_sword(){
	return hasSword;
}

int Player::get_animation(){
	return sprite->animation();
}

int Player::get_current_frame(){
	return sprite->getCurrentFrame();
}

glm::ivec2 Player::get_player_tile(){
	int tilex =(posPlayer.x - posPlayer.x % 32)/32;
	int tiley =(abs(posPlayer.y + 16) - abs(posPlayer.y+ 16) % 64)/64;
	return glm::ivec2(tilex +2, tiley );
}

void Player::hit(int i){
	if (unsheathe){
		lives--;
	}
	else{
		lives = 0;
	}
	if (lives == 0){
		PlaySound(TEXT("sounds/regular_death.wav"), NULL, SND_FILENAME | SND_ASYNC);
		sprite->changeAnimation(DEATH_LEFT + i);
	}
}

bool Player::has_sword() {
	return hasSword;
}

glm::ivec2 Player::get_player_pos(){
	return posPlayer;
}

void Player::setSword(bool has_sword){
	hasSword = has_sword;
}