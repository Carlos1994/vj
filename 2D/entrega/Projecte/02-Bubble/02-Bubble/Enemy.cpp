#pragma comment(lib, "winmm.lib")
#include <cmath>
#include <iostream>
#include <GL/glew.h>
#include <GL/glut.h>
#include "Enemy.h"
#include "Game.h"
#include "Player.h"
#include <windows.h>
#include <mmsystem.h>


#define JUMP_ANGLE_STEP 2
#define JUMP_HEIGHT 96
#define FALL_STEP 3
#define SPRITE 0.05f
#define SPRITE_Y 0.025f


enum EnemyAnims
{
	STAND_LEFT, STAND_RIGHT, RIPOSTE_LEFT, RIPOSTE_RIGHT, MOVE_LEFT, MOVE_RIGHT, ATTACK_LEFT, ATTACK_RIGHT,
	HIT_LEFT, HIT_RIGHT, DEAD_LEFT, DEAD_RIGHT
	
};

const int sprite_pos[12] = { 0, 0, 1, 1, 3, 2, 5, 4, 7, 6, 9, 8};
float pos1;


void Enemy::init(const glm::ivec2 &tileMapPos, ShaderProgram &shaderProgram, Player *player, int num)
{
	bJumping = counter = false;
	spritesheet.loadFromFile("images/enemie"+to_string(num) +".png", TEXTURE_PIXEL_FORMAT_RGBA);
	sprite = Sprite::createSprite(glm::ivec2(64, 64), glm::vec2(SPRITE, SPRITE_Y), &spritesheet, &shaderProgram);
	sprite->setNumberAnimations(12);

	sprite->setAnimationSpeed(STAND_LEFT, 8);
	sprite->addKeyframe(STAND_LEFT, glm::vec2(SPRITE, sprite_pos[STAND_LEFT] * SPRITE_Y));
	sprite->setSpeed(STAND_LEFT, glm::ivec2(0, 0), 1);

	sprite->setAnimationSpeed(STAND_RIGHT, 8);
	sprite->addKeyframe(STAND_RIGHT, glm::vec2(0.f, sprite_pos[STAND_RIGHT] * SPRITE_Y));
	sprite->setSpeed(STAND_RIGHT, glm::ivec2(0, 0), 1);

	sprite->setAnimationSpeed(MOVE_LEFT, 8);
	pos1 = sprite_pos[MOVE_LEFT] * SPRITE_Y;
	for (int i = 4; i >= 0; --i) sprite->addKeyframe(MOVE_LEFT, glm::vec2(i*SPRITE, pos1));
	sprite->setSpeed(MOVE_LEFT, glm::ivec2(0, 0), 2);
	sprite->setSpeed(MOVE_LEFT, glm::ivec2(-1, 0), 2);
	sprite->setSpeed(MOVE_LEFT, glm::ivec2(-2, 0), 2);

	sprite->setAnimationSpeed(MOVE_RIGHT, 8);
	pos1 = sprite_pos[MOVE_RIGHT] * SPRITE_Y;
	for (int i = 0; i < 5; ++i) sprite->addKeyframe(MOVE_RIGHT, glm::vec2(i*SPRITE, pos1));
	sprite->setSpeed(MOVE_RIGHT, glm::ivec2(0, 0), 2);
	sprite->setSpeed(MOVE_RIGHT, glm::ivec2(1, 0), 2);
	sprite->setSpeed(MOVE_RIGHT, glm::ivec2(2, 0), 2);


	sprite->setAnimationSpeed(ATTACK_LEFT, 14);
	pos1 = sprite_pos[ATTACK_LEFT] * SPRITE_Y;
	for (int i = 5; i >= 0; --i) sprite->addKeyframe(ATTACK_LEFT, glm::vec2(i*SPRITE, pos1));
	sprite->setSpeed(ATTACK_LEFT, glm::ivec2(0, 0), 3);
	sprite->setSpeed(ATTACK_LEFT, glm::ivec2(-1, 0), 1);
	sprite->setSpeed(ATTACK_LEFT, glm::ivec2(0, 0), 2);




	sprite->setAnimationSpeed(ATTACK_RIGHT, 14);
	pos1 = sprite_pos[ATTACK_RIGHT] * SPRITE_Y;
	for (int i = 0; i <6; ++i) sprite->addKeyframe(ATTACK_RIGHT, glm::vec2(i*SPRITE, pos1));
	sprite->setSpeed(ATTACK_RIGHT, glm::ivec2(0, 0), 3);
	sprite->setSpeed(ATTACK_RIGHT, glm::ivec2(1, 0), 1);
	sprite->setSpeed(ATTACK_RIGHT, glm::ivec2(0, 0), 2);

	sprite->setAnimationSpeed(RIPOSTE_LEFT, 8);
	pos1 = sprite_pos[RIPOSTE_LEFT] * SPRITE_Y;
	sprite->addKeyframe(RIPOSTE_LEFT, glm::vec2(SPRITE, pos1));
	sprite->setSpeed(RIPOSTE_LEFT, glm::ivec2(0, 0), 1);

	sprite->setAnimationSpeed(RIPOSTE_RIGHT, 8);
	pos1 = sprite_pos[RIPOSTE_RIGHT] * SPRITE_Y;
	sprite->addKeyframe(RIPOSTE_RIGHT, glm::vec2(0.0, pos1));
	sprite->setSpeed(RIPOSTE_RIGHT, glm::ivec2(0, 0), 1);

	sprite->setAnimationSpeed(HIT_LEFT, 8);
	pos1 = sprite_pos[HIT_LEFT] * SPRITE_Y;
	for (int i = 2; i >=0; i-=2) sprite->addKeyframe(HIT_LEFT, glm::vec2(i*SPRITE, pos1));
	sprite->setSpeed(HIT_LEFT, glm::ivec2(0, 0), 3);

	sprite->setAnimationSpeed(HIT_RIGHT, 8);
	pos1 = sprite_pos[HIT_RIGHT] * SPRITE_Y;
	for (int i = 0; i <3; ++i) sprite->addKeyframe(HIT_RIGHT, glm::vec2(i*SPRITE, pos1));
	sprite->setSpeed(HIT_RIGHT, glm::ivec2(0, 0), 3);

	sprite->setAnimationSpeed(DEAD_LEFT, 8);
	sprite->setLoop(DEAD_LEFT, false);
	pos1 = sprite_pos[DEAD_LEFT] * SPRITE_Y;
	for (int i = 5; i >= 0; --i) sprite->addKeyframe(DEAD_LEFT, glm::vec2(i*SPRITE, pos1));
	sprite->setSpeed(DEAD_LEFT, glm::ivec2(0, 0), 6);

	sprite->setAnimationSpeed(DEAD_RIGHT, 8);
	sprite->setLoop(DEAD_RIGHT, false);
	pos1 = sprite_pos[DEAD_RIGHT] * SPRITE_Y;
	for (int i = 0; i < 6; i++) sprite->addKeyframe(DEAD_RIGHT, glm::vec2(i*SPRITE, pos1));
	sprite->setSpeed(DEAD_RIGHT, glm::ivec2(0, 0), 6);



	this->player = player;

	leftMovement = rightMovement = moveXclimb = center = colisionRight = colisionLeft = colisionUp = colisionDown = false;
	climb = 0;
	fallDepth = 1;
	lives = maxLife = 3;
	stunned = 0;
	lives = 4; // 1 mes no se el motiu
	sprite->changeAnimation(8);
	tileMapDispl = tileMapPos;
	//sprite->setPosition(glm::vec2(float(tileMapDispl.x + posPlayer.x), float(tileMapDispl.y + posPlayer.y)));
	sprite->setPosition(tileMapDispl, posPlayer);

}

void Enemy::update(int deltaTime, int xcam, int ycam)
{

	sprite->update(deltaTime);
	if (!bJumping) posPlayer.y += FALL_STEP;

	colisionRight = map->collisionMoveRight(glm::ivec2(posPlayer.x, posPlayer.y), glm::ivec2(32, 64));
	colisionLeft = map->collisionMoveLeft(posPlayer, glm::ivec2(32, 64));
	colisionUp = map->collisionMoveUp(glm::ivec2(posPlayer.x, posPlayer.y + 4), glm::ivec2(32, 64), &climb, center);
	colisionDown = map->collisionMoveDown(glm::ivec2(posPlayer.x - 5, posPlayer.y), glm::ivec2(32, 64), &posPlayer.y);

	bJumping = false;
	//printf("PLAYER ANIMATION : %d \n", player->get_animation());
	if (stunned != 0){
		stunned--;
		if (player->get_animation() == 41 && player->get_current_frame() >6){
			if (get_player_tile().x == player->get_player_tile().x + 1 && get_player_tile().y == player->get_player_tile().y || get_player_tile() == player->get_player_tile()){
				stunned = 0;
				PlaySound(TEXT("sounds/hit.wav"), NULL, SND_FILENAME | SND_ASYNC);
				sprite->changeAnimation(HIT_LEFT);
			}
		}
		else if (player->get_animation() == 40 && player->get_current_frame() > 6){
			if ((get_player_tile().x == player->get_player_tile().x - 1 && get_player_tile().y == player->get_player_tile().y)|| get_player_tile() == player->get_player_tile()){
				stunned = 0;
				PlaySound(TEXT("sounds/hit.wav"), NULL, SND_FILENAME | SND_ASYNC);
				sprite->changeAnimation(HIT_RIGHT);
			}
		}
	}
	else{
		switch (sprite->animation()){
		case STAND_LEFT:
			if (map->cameraOnPos(glm::ivec2(xcam, ycam), posPlayer)){

				if (!player->death()){
					if (get_player_tile().x > player->get_player_tile().x && get_player_tile().y == player->get_player_tile().y){
						sprite->changeAnimation(MOVE_LEFT);
					}
					else sprite->changeAnimation(STAND_RIGHT);
				}
			}

			break;

		case STAND_RIGHT:
			if (map->cameraOnPos(glm::ivec2(xcam, ycam), posPlayer)){
				if (!player->death()){
					if (get_player_tile().x < player->get_player_tile().x && get_player_tile().y == player->get_player_tile().y){
						sprite->changeAnimation(MOVE_RIGHT);
					}
					else{
						sprite->changeAnimation(STAND_LEFT);
					}
				}
			}

			break;

		case MOVE_LEFT:
			//printf("PLAYER POS : %d %d \n", player->get_player_tile().x, player->get_player_tile().y);
			//printf("ENEMI POS : %d %d \n", ((posPlayer.x - (posPlayer.x % 32)) / 32), ((posPlayer.y - (posPlayer.y % 64)) / 64));
			if (colisionLeft){
				sprite->changeAnimation(STAND_RIGHT);
			}
			else if (player->death()){
				sprite->changeAnimation(STAND_LEFT);
			}
			//else if (((posPlayer.x - (posPlayer.x % 32)) / 32) == (player->get_player_tile().x + 1) && ((((posPlayer.y - (posPlayer.y % 64)) / 64) + 1) == (player->get_player_tile().y))){
			else if (get_player_tile().x == player->get_player_tile().x + 1 && get_player_tile().y == player->get_player_tile().y){
				if (posPlayer.x  - player->get_player_pos().x < 40){
					sprite->changeAnimation(ATTACK_LEFT);
				}
			}

			break;

		case MOVE_RIGHT:
			if (colisionRight){
				sprite->changeAnimation(STAND_LEFT);
			}
			else if (player->death()){
				sprite->changeAnimation(STAND_RIGHT);
			}
			//else if (((posPlayer.x - (posPlayer.x % 32)) / 32) == (player->get_player_tile().x - 1) && ((((posPlayer.y - (posPlayer.y % 64)) / 64) + 1) == (player->get_player_tile().y))){
			else if (get_player_tile().x == player->get_player_tile().x -1 && get_player_tile().y == player->get_player_tile().y){
				if (player->get_player_pos().x - posPlayer.x < 40){
					sprite->changeAnimation(ATTACK_RIGHT);
				}
			}
			break;

		case RIPOSTE_LEFT:
			if (sprite->isFinished()){
				sprite->changeAnimation(MOVE_LEFT);
			}
			break;

		case RIPOSTE_RIGHT:

			break;

		case ATTACK_LEFT:
			if (sprite->getCurrentFrame() == 4){
				if (player->get_animation() == 43){
					counter = true;
				}
			}
			else if (sprite->isFinished()){
				stunned = 40;
				if (counter){
					stunned =120;
					counter = false;
					PlaySound(TEXT("sounds/counter.wav"), NULL, SND_FILENAME | SND_ASYNC);
					//printf("COUNTER\n");

				}
				else{
					if ((get_player_tile().x == player->get_player_tile().x + 1 && get_player_tile().y == player->get_player_tile().y) || get_player_tile() == player->get_player_tile()){
						PlaySound(TEXT("sounds/hit_player.wav"), NULL, SND_FILENAME | SND_ASYNC);
						player->hit(1);
					}
				}
				posPlayer.x += 10;
				sprite->changeAnimation(STAND_LEFT);
			}
			break;

		case ATTACK_RIGHT:
			if (sprite->getCurrentFrame() == 4){
				if (player->get_animation() == 42){
					counter = true;
				}
			}
			else if (sprite->isFinished()){
				stunned = 40;
				if (counter){
					stunned = 120;
					counter = false;
					PlaySound(TEXT("sounds/counter.wav"), NULL, SND_FILENAME | SND_ASYNC);
					//printf("COUNTER\n");

				}
				else{
					if ((get_player_tile().x == player->get_player_tile().x - 1 && get_player_tile().y == player->get_player_tile().y )|| get_player_tile() == player->get_player_tile()){
						PlaySound(TEXT("sounds/hit_player.wav"), NULL, SND_FILENAME | SND_ASYNC);
						player->hit(0);

					}
				}
				posPlayer.x -= 10;
				sprite->changeAnimation(STAND_RIGHT);
			}
			break;

			break;

		case HIT_LEFT:
			if (sprite->isFinished()){
				lives--;
				if (lives == 0){
					PlaySound(TEXT("sounds/guard_death_and_obtaining_the_sword.wav"), NULL, SND_FILENAME | SND_ASYNC);
					sprite->changeAnimation(DEAD_LEFT);
				}
				else{
					stunned = 20;
					sprite->changeAnimation(STAND_LEFT);
				}
			}
			break;

		case HIT_RIGHT:
			if (sprite->isFinished()){
				lives--;
				if (lives == 0){
					PlaySound(TEXT("sounds/guard_death_and_obtaining_the_sword.wav"), NULL, SND_FILENAME | SND_ASYNC);
					sprite->changeAnimation(DEAD_RIGHT);
				}
				else{
					stunned = 20;
					sprite->changeAnimation(STAND_RIGHT);
				}
			}

			break;

		}
	}
	//printf("CURRENT FRAME : %d    ANIMACIO: %d\n", sprite->getCurrentFrame(), sprite->animation());
	sprite->setPosition(tileMapDispl, posPlayer);
	//map->checkScreen(posPlayer);
	//map->checkDoors(posPlayer);

}

void Enemy::render()
{
	sprite->render();
}

void Enemy::setTileMap(TileMap *tileMap)
{
	map = tileMap;
	mapSize = map->getMapSize();
}

void Enemy::setPosition(const glm::vec2 &pos)
{
	posPlayer = pos;
	//sprite->setPosition(glm::vec2(float(tileMapDispl.x + posPlayer.x), float(tileMapDispl.y + posPlayer.y)));
	sprite->setPosition(tileMapDispl, posPlayer);
}

glm::ivec2 Enemy::get_player_tile(){
	int tilex = (posPlayer.x - posPlayer.x % 32) / 32;
	int tiley = (abs(posPlayer.y + 16) - abs(posPlayer.y + 16) % 64) / 64;
	return glm::ivec2(tilex + 2, tiley);
}

glm::ivec2 Enemy::get_player_pos(){
	return posPlayer;
}

int Enemy::get_lives(){
	return lives;
}

int Enemy::get_max_lives(){
	return maxLife;
}

void Enemy::set_lives(int lives){
	this->lives = lives +1;
	maxLife = lives;
}