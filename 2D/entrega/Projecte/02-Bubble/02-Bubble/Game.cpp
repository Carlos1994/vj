#include <GL/glew.h>
#include <GL/glut.h>
#include "Game.h"


void Game::init()
{
	bPlay = true;
	glClearColor(0.3f, 0.3f, 0.3f, 1.0f);
	initWindow = true;
	restartGame = false;
	mainMenu = true;
	changeLevel = false;
	zPressed = false;
	level = 1;
	actualLevel = 1;
	scene.init(level);
}

bool Game::update(int deltaTime)
{

		if ((scene.restartGame() && restartGame)|| changeLevel) {
			scene.free();
			initWindow = false;
			changeLevel = false;
			scene.init(level);
		}
	scene.update(deltaTime);
	
	return bPlay;
}

void Game::render()
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	scene.render(zPressed);
}

void Game::keyPressed(int key)
{
	//printf("KEY : %d \n", key);
	if (key == 13) restartGame = true;
	if (key == 122 && scene.playing()) zPressed = !zPressed;
	//if(key == 27) // Escape code
		//bPlay = false;
	keys[key] = true;
}

void Game::keyReleased(int key)
{
	if (key == 13) restartGame = false;
	keys[key] = false;
}

void Game::specialKeyPressed(int key)
{
	specialKeys[key] = true;
}

void Game::specialKeyReleased(int key)
{
	specialKeys[key] = false;
}

void Game::mouseMove(int x, int y)
{
}

void Game::mousePress(int button)
{
}

void Game::mouseRelease(int button)
{
}

bool Game::getKey(int key) const
{
	return keys[key];
}

bool Game::getSpecialKey(int key) const
{
	return specialKeys[key];
}

void Game::setGameState(bool state){
	bPlay = state;
}

void Game::nextLevel(){
	level++;
	if (level > 2) level = 2;
	changeLevel = true;
	actualLevel = level;
}

void Game::levelPlus(){
	actualLevel+=1;
	changeLevel = true;
	level++;
	if (level > 2) level = 2;
	if (actualLevel >2){
		//printf("HE ENTRAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAT %d\n", actualLevel);
		scene.setCredits();
		initWindow = true;
		restartGame = false;
		mainMenu = true;
		changeLevel = false;
		level = 1;
		actualLevel = 1;
	}
}

void Game::setZoom(bool z) {
	zPressed = false;
}


