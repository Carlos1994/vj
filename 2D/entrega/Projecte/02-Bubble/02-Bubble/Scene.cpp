#pragma comment(lib, "winmm.lib")
#include <iostream>
#include <cmath>
#include <glm/gtc/matrix_transform.hpp>
#include "Scene.h"
#include "Game.h"
#include <windows.h>
#include <mmsystem.h>


#define SCREEN_X 0
#define SCREEN_Y 0

#define INIT_PLAYER_X_TILES 61
#define INIT_PLAYER_Y_TILES 1


Scene::Scene()
{
	//PlaySound(TEXT("sounds/won.wav"), NULL, SND_FILENAME | SND_ASYNC);
	map = NULL;
	player = NULL;
	torch = NULL;
	column = NULL;
	enemy = NULL;
	enemy2 = NULL;
	iniWindow = true;
	initialWindow = NULL;
	credits = false;
	creditsMenu = NULL;
	pause = false;
	pauseMenu = NULL;
	menu = true;
	mainMenu = NULL;
	arrowMenu = NULL;
	lives_bar = NULL;
	empty_life = NULL;
	full_life = NULL;
	empty_life_e = NULL;
	full_life_e = NULL;
	empty_life_e2 = NULL;
	full_life_e2 = NULL;
	swordP = NULL;
	potion = NULL;
	zoomActive = false;
	counter = 0;
	timeZoom = 0;
}

Scene::~Scene()
{
	if(map != NULL)
		delete map;
	if(player != NULL)
		delete player;
	if (torch != NULL)
		delete torch;
	if (column != NULL)
		delete column;
	if (enemy != NULL)
		delete enemy;
	if (enemy2 != NULL)
		delete enemy2;
	if (initialWindow != NULL)
		delete initialWindow;
	if (creditsMenu != NULL)
		delete creditsMenu;
	if (pauseMenu != NULL)
		delete pauseMenu;
	if (mainMenu != NULL)
		delete mainMenu;
	if (arrowMenu != NULL)
		delete arrowMenu;
	if (lives_bar != NULL)
		delete lives_bar;
	if (empty_life != NULL)
		delete empty_life;
	if (full_life != NULL)
		delete full_life;
	if (empty_life_e != NULL)
		delete empty_life_e;
	if (full_life_e != NULL)
		delete full_life_e;
	if (empty_life_e2 != NULL)
		delete empty_life_e2;
	if (full_life_e2 != NULL)
		delete full_life_e2;
	if (potion != NULL) {
		delete potion;
	}
	/*if (swordP != NULL)
		delete swordP;*/
}


void Scene::init(int level)
{
	
	//iniWindow = true;
	//Initial camera's position
	posScreenX = SCREEN_WIDTH + 32 - 1;
	posScreenY = SCREEN_HEIGHT - 1;
	downPressed = 0;
	timeSum = 0;
	initShaders();
	map = TileMap::createTileMap("levels/level" + to_string(level) + ".txt", glm::vec2(SCREEN_X, SCREEN_Y), texProgram, level);
	map->setScreen(posScreenX, posScreenY);

	glm::ivec2 nTiles = map->getMapSize();
	ntilesX = nTiles.x;
	ntilesY = nTiles.y;
	map->getObjects(&torchs, &columns, &thorns, &potions);

	/*for (unsigned int i = 0; i < torchs.size(); ++i) printf("Torch at %d\n", torchs[i]);
	for (unsigned int i = 0; i < columns.size(); ++i) printf("Column at %d\n", columns[i]);*/

	//Initial window
	initialWindow = new Object();
	initialWindow->setFile("images/init.png");
	initialWindow->setNumberAnimations(5);
	initialWindow->setTileSize(320, 200);
	initialWindow->setSpeed(1);
	initialWindow->init(glm::ivec2(0, 0), texProgram);
	initialWindow->repeatAnimations(5);
	initialWindow->setPosition(glm::ivec2(0, 0));
	initialWindow->setLoop(false);

	//main Menu
	mainMenu = new Object();
	mainMenu->setFile("images/menu.png");
	mainMenu->setNumberAnimations(1);
	mainMenu->setTileSize(320, 200);
	mainMenu->init(glm::ivec2(0, 0), texProgram);
	mainMenu->setPosition(glm::ivec2(0, 0));
	mainMenu->setLoop(false);

	//credits Menu
	creditsMenu = new Object();
	creditsMenu->setFile("images/credits.png");
	creditsMenu->setNumberAnimations(1);
	creditsMenu->setTileSize(320, 200);
	creditsMenu->init(glm::ivec2(0, 0), texProgram);
	creditsMenu->setPosition(glm::ivec2(0, 0));
	creditsMenu->setLoop(false);


	//pause Menu
	pauseMenu = new Object();
	pauseMenu->setFile("images/pause_menu.png");
	pauseMenu->setNumberAnimations(1);
	pauseMenu->setTileSize(320, 200);
	pauseMenu->init(glm::ivec2(0, 0), texProgram);
	pauseMenu->setPosition(glm::ivec2(0, 0));
	pauseMenu->setLoop(false);

	//arrow menu
	arrowMenu = new Object();
	arrowMenu->setFile("images/arrow_menu.png");
	arrowMenu->setNumberAnimations(1);
	arrowMenu->setTileSize(20, 20);
	arrowMenu->init(glm::ivec2(0, 0), texProgram);
	arrowMenu->setPosition(glm::ivec2(80, 90));
	arrowMenu->setLoop(false);

	//Player
	player = new Player();
	player->init(glm::ivec2(SCREEN_X, SCREEN_Y), texProgram);
	player->setPosition(glm::vec2(((INIT_PLAYER_X_TILES) - ((level -1)* 14)) * 32, ((INIT_PLAYER_Y_TILES-1) * map->getTileSize())));
	player->setTileMap(map);
	if(level == 2) player->setSword(true);
	else player->setSword(false);

	//enemy
	enemy = new Enemy();
	enemy->init(glm::ivec2(SCREEN_X, SCREEN_Y) , texProgram, player,1);
	enemy->setPosition(glm::vec2(((INIT_PLAYER_X_TILES + 15) - ((level - 1) * 11)) * 32, ((INIT_PLAYER_Y_TILES + 2 -((level - 1) * 1)) * map->getTileSize())));
	enemy->setTileMap(map);
	enemy->set_lives(3);

	enemy2 = new Enemy();
	enemy2->init(glm::ivec2(SCREEN_X, SCREEN_Y), texProgram, player,2);
	enemy2->setPosition(glm::vec2(((INIT_PLAYER_X_TILES - 15) - ((level - 1) * -10)) * 32, ((INIT_PLAYER_Y_TILES - 2) + ((level - 1) * 1)) * map->getTileSize()));
	enemy2->setTileMap(map);
	enemy2->set_lives(5);

	//Torch
	torch = new Object();
	torch->setFile("images/torch.png");
	torch->setNumberAnimations(3);
	torch->setTileSize(32, 32);
	torch->init(glm::ivec2(SCREEN_X, SCREEN_Y), texProgram);
	torch->setPosition(glm::ivec2(106, 66));

	//Column
	column = new Object();
	column->setFile("images/column" + to_string(level) + ".png");
	column->setNumberAnimations(1);
	column->setTileSize(64, 64);
	column->init(glm::ivec2(SCREEN_X, SCREEN_Y), texProgram);
	column->setPosition(glm::ivec2(128, 64));

	//Thorn
	thorn = new Object();
	thorn->setFile("images/punxes.png");
	thorn->setNumberAnimations(12);
	thorn->setTileSize(64, 64);
	thorn->setSpeed(4);
	thorn->init(glm::ivec2(SCREEN_X, SCREEN_Y), texProgram);
	thorn->setPosition(glm::ivec2(128, 64));


	//Lives
	lives_bar = new Object();
	lives_bar->setFile("images/black_bar.png");
	lives_bar->setNumberAnimations(1);
	lives_bar->setTileSize(320, 8);
	lives_bar->init(glm::ivec2(SCREEN_X, SCREEN_Y), texProgram);
	lives_bar->setPosition(glm::ivec2(0, 192));

	full_life = new Object();
	full_life->setFile("images/life_full.png");
	full_life->setNumberAnimations(1);
	full_life->setTileSize(8, 8);
	full_life->init(glm::ivec2(SCREEN_X, SCREEN_Y), texProgram);
	full_life->setPosition(glm::ivec2(0, 192));

	empty_life = new Object();
	empty_life->setFile("images/life_empty.png");
	empty_life->setNumberAnimations(1);
	empty_life->setTileSize(8, 8);
	empty_life->init(glm::ivec2(SCREEN_X, SCREEN_Y), texProgram);
	empty_life->setPosition(glm::ivec2(0, 192));

	full_life_e = new Object();
	full_life_e->setFile("images/life_full_e.png");
	full_life_e->setNumberAnimations(1);
	full_life_e->setTileSize(8, 8);
	full_life_e->init(glm::ivec2(SCREEN_X, SCREEN_Y), texProgram);
	full_life_e->setPosition(glm::ivec2(0, 192));

	empty_life_e = new Object();
	empty_life_e->setFile("images/life_empty_e.png");
	empty_life_e->setNumberAnimations(1);
	empty_life_e->setTileSize(8, 8);
	empty_life_e->init(glm::ivec2(SCREEN_X, SCREEN_Y), texProgram);
	empty_life_e->setPosition(glm::ivec2(0, 192));

	full_life_e2 = new Object();
	full_life_e2->setFile("images/life_full_e2.png");
	full_life_e2->setNumberAnimations(1);
	full_life_e2->setTileSize(8, 8);
	full_life_e2->init(glm::ivec2(SCREEN_X, SCREEN_Y), texProgram);
	full_life_e2->setPosition(glm::ivec2(0, 192));

	empty_life_e2 = new Object();
	empty_life_e2->setFile("images/life_empty_e2.png");
	empty_life_e2->setNumberAnimations(1);
	empty_life_e2->setTileSize(8, 8);
	empty_life_e2->init(glm::ivec2(SCREEN_X, SCREEN_Y), texProgram);
	empty_life_e2->setPosition(glm::ivec2(0, 192));

	//Potion
	potion = new Object();
	potion->setFile("images/potion.png");
	potion->setNumberAnimations(10);
	potion->setTileSize(32, 64);
	potion->setSpeed(8);
	potion->init(glm::ivec2(0, 0), texProgram);
	potion->setPosition(glm::ivec2(0, 0));

	//Sword
	swordP = map->getSword();

	//Camera
	if (level == 2) {
		int screenX = ((INIT_PLAYER_X_TILES)-((level - 1) * 14)) * 32;
		screenX = ((((screenX)* 32) + 319) / 320) * 320 + 32;
		int screenY = ((((INIT_PLAYER_Y_TILES - 1) * 64) + 192) / 192) * 192;
		posScreenX = screenX;
		posScreenY = screenY;
		changeCamera();
	}	
	else projection = glm::ortho(float(0), float(320), float(200), 0.f);
	currentTime = 0.0f;
}

void Scene::update(int deltaTime)
{
	currentTime += deltaTime;
	//printf("DELTA TIME : %d \n", deltaTime);

	if (pause){
		if (Game::instance().getKey(98) || Game::instance().getKey(66)){
			pause = false;
		}
		else if (Game::instance().getKey(27)){
			Game::instance().setGameState(false);
		}

	}
	else if (credits){
		if (Game::instance().getKey(98) || Game::instance().getKey(66)){
			credits = false;
		}
		else if (Game::instance().getKey(27)){
			Game::instance().setGameState(false);
		}

	}
	else if (menu){
		mainMenu->update(deltaTime);

		if (Game::instance().getKey(27)) Game::instance().setGameState(false);
		else if (Game::instance().getSpecialKey(GLUT_KEY_DOWN)){
			if (timeSum == 0 || (timeSum +500) <= glutGet(GLUT_ELAPSED_TIME)){
				downPressed++;
				arrowMenu->setPosition(glm::ivec2(80, 90 + (abs(downPressed) % 3) * 20));
				timeSum = glutGet(GLUT_ELAPSED_TIME);
			}
		}
		
		else if (Game::instance().getSpecialKey(GLUT_KEY_UP)){
			if (timeSum == 0 || (timeSum + 500) <= glutGet(GLUT_ELAPSED_TIME)){
				downPressed--;
				if (downPressed < 0) downPressed = 0;
				arrowMenu->setPosition(glm::ivec2(80, 90 + (abs(downPressed) % 3) * 20));
				timeSum = glutGet(GLUT_ELAPSED_TIME);
			}
		}

		else if (Game::instance().getKey(13)){
			if (downPressed % 3 == 0){
				menu = false;
				PlaySound(TEXT("sounds/won.wav"), NULL, SND_FILENAME | SND_ASYNC);
			}
			else if (downPressed % 3 == 1){
				pause = true;
			}
			else{
				credits = true;
			}
		}
		arrowMenu->update(deltaTime);


	}
	else{
		if (Game::instance().getKey(32)){
			PlaySound(TEXT("sound/empty_sound.wav"), NULL, SND_FILENAME | SND_ASYNC);
			iniWindow = false;
		}
		if (Game::instance().getKey(112)){
			pause = true;
			if (iniWindow){
				pauseMenu->setPosition(glm::ivec2(posScreenX - 352, posScreenY - 200));
			}
			else{
				pauseMenu->setPosition(glm::ivec2(posScreenX - 320, posScreenY - 192));
			}
			pauseMenu->update(deltaTime);
		}
		else if (iniWindow){
			initialWindow->update(deltaTime);

		}
		else {
			//PlaySound(NULL, NULL, SND_FILENAME | SND_ASYNC);
			player->update(deltaTime);
			enemy->update(deltaTime, posScreenX, posScreenY);
			enemy2->update(deltaTime, posScreenX, posScreenY);
			torch->update(deltaTime);
			column->update(deltaTime);
			if (!player->death()) thorn->update(deltaTime);
			map->updateObjects(deltaTime);
			lives_bar->update(deltaTime);
			full_life->update(deltaTime);
			empty_life->update(deltaTime);
			swordP->update(deltaTime);
			potion->update(deltaTime);
			/*if (player->death()) {
				init();
				iniWindow = true;
			}*/
		}
	}
}

void Scene::render(bool zoom)
{
	glm::mat4 modelview;
	texProgram.use();
	texProgram.setUniformMatrix4f("projection", projection);
	texProgram.setUniform4f("color", 1.0f, 1.0f, 1.0f, 1.0f);
	modelview = glm::mat4(1.0f);
	texProgram.setUniformMatrix4f("modelview", modelview);
	texProgram.setUniform2f("texCoordDispl", 0.f, 0.f);
	if (pause){
		pauseMenu->render();
	}
	else if (credits){
		creditsMenu->render();
	}
	else if (menu){
		mainMenu->render();
		arrowMenu->render();
	}
	else{
		if (iniWindow) {
			//PlaySound(TEXT("sound/empty_sound.wav"), NULL, SND_FILENAME | SND_ASYNC);
			initialWindow->render();
			
		}
		else {
			if (zoom && (glutGet(GLUT_ELAPSED_TIME)- timeZoom) >= 200) {
				timeZoom = glutGet(GLUT_ELAPSED_TIME);
				int x = (30 + 1 * counter) * 32;
				if (x > (ntilesX * 32)) {
					Game::instance().setZoom(false);
					counter = 0;
				}
				projection = glm::ortho(float(x - (320 * 3)), float(x), float(ntilesY * 64), float(0));
				texProgram.setUniformMatrix4f("projection", projection);
				++counter;
			}
			else if (!zoom){
				counter = 0;
				changeCamera();
				texProgram.setUniformMatrix4f("projection", projection);
			}

			//Mirem si hem de moure la camara
			glm::ivec2 posMap = map->getScreen();
			if (posMap.x != posScreenX || posMap.y != posScreenY) {
				posScreenX = posMap.x;
				posScreenY = posMap.y;
				changeCamera();
			}

			/*glm::mat4 modelview;
			texProgram.use();
			texProgram.setUniformMatrix4f("projection", projection);
			texProgram.setUniform4f("color", 1.0f, 1.0f, 1.0f, 1.0f);
			modelview = glm::mat4(1.0f);
			texProgram.setUniformMatrix4f("modelview", modelview);
			texProgram.setUniform2f("texCoordDispl", 0.f, 0.f);*/
			map->render();

			/*if (map->getLevel() != level) {
				level = map->getLevel();
				map->getObjects(&torchs, &columns);
				}*/

			map->renderObjects();
			if (!player->has_sword()) swordP->render();


			int row, col;
			//Torchs
			for (unsigned int i = 0; i < torchs.size(); ++i) {
				row = torchs[i] / ntilesX;
				col = (torchs[i] % ntilesX);
				torch->setPosition(glm::ivec2(col * 32 + 42, row * 64 + 2));
				torch->render();
			}

			for (unsigned int i = 0; i < thorns.size(); ++i) {
				row = thorns[i] / ntilesX;
				col = thorns[i] % ntilesX;
				thorn->setPosition(glm::ivec2(col * 32, row * 64));
				thorn->render();
			}

			player->render();
			enemy->render();
			enemy2->render();

			//Potions
			map->getPotions(&potions);
			for (unsigned int i = 0; i < potions.size(); ++i) {
				potion->setPosition(glm::ivec2((potions[i] % ntilesX) * 32, (potions[i] / ntilesX) * 64));
				potion->render();
			}

			//Columns
			for (unsigned int i = 0; i < columns.size(); ++i) {
				row = columns[i] / ntilesX;
				col = (columns[i] % ntilesX);
				column->setPosition(glm::ivec2(col * 32, row * 64));
				column->render();
			}

			lives_bar->setPosition(glm::ivec2(posScreenX - 320, posScreenY));
			lives_bar->render();
			int n = player->get_lives();
			int max = player->get_max_lives();
			//printf("Lives = %d\n\n\n\n\n", n);
			for (int i = 0; i < max; ++i) {
				empty_life->setPosition(glm::ivec2(posScreenX - 320 + i * 8, posScreenY));
				empty_life->render();
			}
			for (int i = 0; i < n; ++i) {
				full_life->setPosition(glm::ivec2(posScreenX - 320 + i * 8, posScreenY));
				full_life->render();
			}
			if (map->cameraOnPos(glm::ivec2(posScreenX, posScreenY), enemy->get_player_pos())){
				int n = enemy->get_lives();
				int max = enemy->get_max_lives();
				//printf("Lives = %d\n\n\n\n\n", n);
				for (int i = 0; i < max; ++i) {
					empty_life_e->setPosition(glm::ivec2((posScreenX - i * 8) - 8, posScreenY));
					empty_life_e->render();
				}
				for (int i = 0; i < n; ++i) {
					full_life_e->setPosition(glm::ivec2((posScreenX - i * 8) - 8, posScreenY));
					full_life_e->render();
				}
			}
			if (map->cameraOnPos(glm::ivec2(posScreenX, posScreenY), enemy2->get_player_pos())){
				int n = enemy2->get_lives();
				int max = enemy2->get_max_lives();
				//printf("Lives = %d\n\n\n\n\n", n);
				for (int i = 0; i < max; ++i) {
					empty_life_e2->setPosition(glm::ivec2((posScreenX - i * 8) - 8, posScreenY));
					empty_life_e2->render();
				}
				for (int i = 0; i < n; ++i) {
					full_life_e2->setPosition(glm::ivec2((posScreenX - i * 8) - 8, posScreenY));
					full_life_e2->render();
				}
			}
		}
	}
	

}

void Scene::initShaders()
{
	Shader vShader, fShader;

	vShader.initFromFile(VERTEX_SHADER, "shaders/texture.vert");
	if(!vShader.isCompiled())
	{
		cout << "Vertex Shader Error" << endl;
		cout << "" << vShader.log() << endl << endl;
	}
	fShader.initFromFile(FRAGMENT_SHADER, "shaders/texture.frag");
	if(!fShader.isCompiled())
	{
		cout << "Fragment Shader Error" << endl;
		cout << "" << fShader.log() << endl << endl;
	}
	texProgram.init();
	texProgram.addShader(vShader);
	texProgram.addShader(fShader);
	texProgram.link();
	if(!texProgram.isLinked())
	{
		cout << "Shader Linking Error" << endl;
		cout << "" << texProgram.log() << endl << endl;
	}
	texProgram.bindFragmentOutput("outColor");
	vShader.free();
	fShader.free();
}

void Scene::changeCamera() {
	projection = glm::ortho(float(posScreenX-320), float(posScreenX), float(posScreenY+8), float(posScreenY - 192));
}

bool Scene::restartGame() {
	return player->death();
}

void Scene::free(){
	if (map != NULL)
		delete map;
	if (player != NULL)
		delete player;
	if (torch != NULL)
		delete torch;
	if (column != NULL)
		delete column;
	if (enemy != NULL)
		delete enemy;
	if (initialWindow != NULL)
		delete initialWindow;
	if (creditsMenu != NULL)
		delete creditsMenu;
	if (pauseMenu != NULL)
		delete pauseMenu;
	if (mainMenu != NULL)
		delete mainMenu;
	if (arrowMenu != NULL)
		delete arrowMenu;
	if (lives_bar != NULL)
		delete lives_bar;
	if (empty_life != NULL)
		delete empty_life;
	if (full_life != NULL)
		delete full_life;
	if (empty_life_e != NULL)
		delete empty_life_e;
	if (full_life_e != NULL)
		delete full_life_e;
}

void Scene::setCredits(){
	credits = true;
	menu = true;
	iniWindow = true;
	PlaySound(TEXT("sounds/won.wav"), NULL, SND_FILENAME | SND_ASYNC);
	free();
	init(1);
}

bool Scene::playing() {
	return (!iniWindow || !mainMenu);
}