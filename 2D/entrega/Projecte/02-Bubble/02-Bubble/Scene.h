#ifndef _SCENE_INCLUDE
#define _SCENE_INCLUDE


#include <glm/glm.hpp>
#include "ShaderProgram.h"
#include "TileMap.h"
#include "Player.h"
#include "Object.h"
#include "Enemy.h"


// Scene contains all the entities of our game.
// It is responsible for updating and render them.


class Scene
{

public:
	Scene();
	~Scene();

	void init(int level);
	void update(int deltaTime);
	void render(bool zoom);
	void changeCamera();
	bool restartGame();
	void free();
	void setCredits();
	bool playing();

private:
	void initShaders();

private:
	TileMap *map;
	Player *player;
	Enemy *enemy, *enemy2;
	Object *torch, *column, *thorn, *initialWindow, *mainMenu, *pauseMenu, *creditsMenu, *arrowMenu, *lives_bar, *empty_life, *full_life, *swordP, *empty_life_e, *full_life_e, *empty_life_e2, *full_life_e2, *potion;
	ShaderProgram texProgram;
	float currentTime;
	glm::mat4 projection;

	vector<int> torchs, columns, thorns, potions;
	string level = "levels/level01_1.txt";

	int posScreenX, posScreenY;
	int ntilesX, ntilesY;
	int downPressed;
	unsigned long int timeSum;
	bool iniWindow, menu, pause, credits, zoomActive;

	int counter;

	unsigned long int timeZoom;



};


#endif // _SCENE_INCLUDE

