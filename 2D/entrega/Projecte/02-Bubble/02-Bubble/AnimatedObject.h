#ifndef _ANIMATED_OBJECT_INCLUDE
#define _ANIMATED_OBJECT_INCLUDE

#include "Sprite.h"

class AnimatedObject {

public:
	void init(const glm::ivec2 &tileMapPos, ShaderProgram &shaderProgram);
	void update(int deltaTime);
	void render();

	void setPosition(const glm::ivec2 &pos);
	glm::ivec2 getPosition();
	void setNumberAnimations(int n);
	void setFile(const string file);
	void setTileSize(int sizeX, int sizeY);
	void changeAnimation(int anim);
	int getAnimation();
	void setSpeed(int s);
	bool animationFinished();

private:
	glm::ivec2 tileMapDispl, posObject;
	Texture spritesheet;
	Sprite *sprite;
	int animations, currentAnimation;
	string image;

	unsigned long time;

	int tileSizeX, tileSizeY;
	int speed = 2;
};

#endif