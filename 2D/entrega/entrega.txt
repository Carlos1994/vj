PRINCE OF PERSIA:
Pau Vilanova Planas
Carlos L�rida Mezcua

FUNCIONALITATS IMPLEMENTADES:

El joc parteix d'un men� principal, el qual es composa de play, instructions i credits. 
Al seleccionar play es dona pas a la hist�ria del joc, i un cop premut l'espai la partida comen�a.
En qualsevol moment es pot apretar la tecla p per mostrar les instruccions a l'hora que serveix de men� de pausa.
Per tornar enrera tant al men� de pausa com als credits s'ha d'apretar la tecla b (back), estant en ambd�s finestres
si es presiona la tecla ESC el joc es tanca.
El joc es guanya si el pr�ncep aconsegueix arribar al final del segon nivell, un cop aconseguit es donar� pas als 
cr�dits amb la opci� de tornar al menu principal i tornar a jugar.
Si el pr�ncep mor, es pot pr�mer ENTER per retornar al punt d'origen del nivell en que es troba.

PART COMUNA:

S'ha intentat fer el joc el m�s semblant possible al prince of persia de 1989 tot i que els sprites del pr�ncep i 
dels enemics s�n de una versi� diferent (s�n els proporcionats per fer el projecte).
Per m�s semblant possible entenem que els mapes han de ser id�ntics al propi joc, per tal de fer un tribut.
Per tant el primer nivell correspon tamb� al primer nivell del joc original, i el segon nivell del nostre projecte
correspon al nivell quatre del joc original (ja que es demanava un segon nivell d'aspecte diferent).

Per les trampes hem implementat les punxes que surten del terra, i les cel�les que cauen al terra al passar-hi el 
pr�ncep per sobre. 
Hem dissenyat 2 enemics, un de color lila i l'altre groc, el segon amb m�s vides respecte el primer. Es troben a les mateixes
posicions que el joc original.
En el nostre projecte el pr�ncep pot lluitar contre els enemics de la seg�ent manera:
Per treure l'espasa s'ha de premer la tecla s (sword).
S'ha desperar que l'enemic inicii l'atac, i el jugador ha de fer el moviment de protegir, despr�s l'enemic estar� atordit
el qual el jugador pot aprofitar per atacar. 
Es protegeix apretant la tecla de direcci� oposada a la que el jugador mira, i per atacar s'ha de pr�mer la tecla en la mateixa direcci�.

TRES FUNCIONALITATS DEL JOC:

S'ha fet un men� principal el qual s'ha explicat anteriorment.
El joc cont� sons per la majoria d'animacions del personatge, de l'entorn i del combat.
S'han animat les torxes, les trampes, les pocions, les portes, les cel�les que activen les portes, i les portes finals de cada nivell.


PART CREATIVA:

Que seria un joc persa, sense una l�mpara? 
Per aix� hem pensat en afegir una animaci� addicional del princep la qual es treu una l�mpara de la butxaca, la frega i es transforma 
en una serp mentres s'envolta de fum. No �s tot, al transformar-se en serp pot atrevessar les portes en forma de gabia a trav�s dels forats!
Aquesta funcionalitat adicional dota de menys dificultat el joc, per� �s molt divertida.
El princep es transforma apretant la tecla o (de opcional).


Per passar-te el primer nivell el que s'ha de fer es el mateix que el joc original; s'ha d'anar a buscar l'espassa per poder derrotar l'enemic
que t'impedeix l'entrada al segon nivell.
Pero hem dotat el joc de un cheat el qual puguis anar al segon nivell. Es apretant la tecla l (level), fen aix� passes al segon nivell tal com 
si haguessis atrevessat la porta final del nivell anterior.

Tamb� donem la oportunitat de mirar tot el mapa amb la c�mera, sols apretant z (zoom) en qualsevol moment del nivell, la camara fa un zoom enrera
despla�ant-se i mostrant tot el mapa.

V�DEO:

Adjuntem m�s d'un v�deo per a poder mostrar totes les funcionalitats implementades.