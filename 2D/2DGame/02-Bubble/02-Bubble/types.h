#ifndef _TYPES
#define _TYPES

#include <vector>
using namespace std;

struct RelatedObjects {
	vector<glm::ivec2> fElement;
	vector<glm::ivec2> sElement;
	bool open = false;
};

struct BrokenGround {
	glm::ivec2 pos;
	bool visited = false;
	bool broken = false;
	unsigned long int time = 0;
};

#endif
