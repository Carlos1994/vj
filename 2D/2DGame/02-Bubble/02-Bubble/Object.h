#ifndef _OBJECT_INCLUDE
#define _OBJECT_INCLUDE

#include "Sprite.h"

class Object
{

public:

	void init(const glm::ivec2 &tileMapPos, ShaderProgram &shaderProgram);
	void update(int deltaTime);
	void render();

	void setPosition(const glm::ivec2 &pos);
	void setNumberAnimations(int n);
	void setFile(const string file);
	void setTileSize(int sizeX, int sizeY);
	void setSpeed(int speed);
	void repeatAnimations(int times);
	bool isFinished();
	void setLoop(bool repeat);
	void setMovement(const glm::ivec2 &move);
	glm::ivec2 getPosition();
	void setVisible(bool render);
	void changeAnimation(int anim);

private:
	glm::ivec2 tileMapDispl, posObject;
	Texture spritesheet;
	Sprite *sprite;
	int animations;
	string image;
	
	glm::ivec2 speed_vector = glm::ivec2(0, 0);
	bool visible = true;

	int tileSizeX, tileSizeY;
	int speed = 8;
};

#endif

