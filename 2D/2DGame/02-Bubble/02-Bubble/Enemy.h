#ifndef _ENEMY_INCLUDE
#define _ENEMY_INCLUDE


#include "Sprite.h"
#include "TileMap.h"
#include "Player.h"


// Player is basically a Sprite that represents the player. As such it has
// all properties it needs to track its movement, jumping, and collisions.


class Enemy
{

public:
	void init(const glm::ivec2 &tileMapPos, ShaderProgram &shaderProgram, Player *player, int num);
	void update(int deltaTime, int xcam, int ycam);
	void render();
	glm::ivec2 get_player_pos();
	void setTileMap(TileMap *tileMap);
	void setPosition(const glm::vec2 &pos);
	glm::ivec2 get_player_tile();
	int get_lives();
	void set_lives(int lives);
	int get_max_lives();

private:
	bool bJumping, moveXclimb, center, colisionRight, colisionLeft, colisionUp, colisionDown;
	glm::ivec2 tileMapDispl, posPlayer, mapSize;
	int jumpAngle, startY;
	Texture spritesheet;
	Sprite *sprite;
	TileMap *map;
	Player *player;
	bool rightMovement, leftMovement, counter;
	int climb, fallDepth, lives, stunned, maxLife;

};


#endif // _ENEMIE_INCLUDE


