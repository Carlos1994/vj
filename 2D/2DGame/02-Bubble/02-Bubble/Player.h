#ifndef _PLAYER_INCLUDE
#define _PLAYER_INCLUDE


#include "Sprite.h"
#include "TileMap.h"


// Player is basically a Sprite that represents the player. As such it has
// all properties it needs to track its movement, jumping, and collisions.


class Player
{

public:
	void init(const glm::ivec2 &tileMapPos, ShaderProgram &shaderProgram);
	void update(int deltaTime);
	void render();
	bool death();
	bool player_sword();
	int get_lives();
	int get_max_lives();
	int get_animation();
	int get_current_frame();
	glm::ivec2 get_player_tile();
	void setTileMap(TileMap *tileMap);
	void setPosition(const glm::vec2 &pos);
	void hit(int i);
	void setSword(bool has_sword);
	glm::ivec2 get_player_pos();

	bool has_sword();
	
private:
	bool bJumping, moveXclimb, center, colisionRight, colisionLeft, colisionUp, colisionDown, sound, haEscalat, canvi_lvl, canvi;
	glm::ivec2 tileMapDispl, posPlayer;
	int jumpAngle, startY;
	Texture spritesheet;
	Sprite *sprite;
	TileMap *map;
	bool rightMovement, leftMovement, hasSword, unsheathe;
	int climb, fallDepth, lives, maxLife, tileDoorX;

};


#endif // _PLAYER_INCLUDE


