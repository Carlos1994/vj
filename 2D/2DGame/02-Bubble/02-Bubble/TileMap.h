#ifndef _TILE_MAP_INCLUDE
#define _TILE_MAP_INCLUDE

#include <glm/glm.hpp>
#include "Texture.h"
#include "ShaderProgram.h"
#include <vector>
#include "types.h"
#include "AnimatedObject.h"
#include "Object.h"
#include <GL/glut.h>


// Class Tilemap is capable of loading a tile map from a text file in a very
// simple format (see level01.txt for an example). With this information
// it builds a single VBO that contains all tiles. As a result the render
// method draws the whole map independently of what is visible.


class TileMap
{

public:
	// Tile maps can only be created inside an OpenGL context
	static TileMap *createTileMap(const string &levelFile, const glm::vec2 &minCoords, ShaderProgram &program, int level);

	TileMap(const string &levelFile, const glm::vec2 &minCoords, ShaderProgram &program, int level);
	~TileMap();

	void render() const;
	void free();
	
	int getTileSize() const { return tileSize; }
	int getJumpTile(int x, int y);
	bool collisionMoveLeft(const glm::ivec2 &pos, const glm::ivec2 &size) const;
	bool collisionMoveRight(const glm::ivec2 &pos, const glm::ivec2 &size) const;
	bool collisionMoveDown(const glm::ivec2 &pos, const glm::ivec2 &size, int *posY);
	bool collisionMoveUp(const glm::ivec2 &pos, const glm::ivec2 &size, int *climb, bool &center) const;
	bool cameraOnPos(const glm::ivec2 &cam, const glm::ivec2 &posEnemie);

	void getObjects(vector<int> *v1, vector<int> *v2, vector<int> *v3, vector<int> *v4);
	string getLevel();
	void setScreen(int posX, int posY);
	glm::ivec2 getScreen();
	void checkScreen(const glm::ivec2 &pos);
	glm::ivec2 getMapSize();
	int getTile(glm::ivec2 pos);
	int getTileFromTile(glm::ivec2 tile);
	void checkDoors(const glm::ivec2 &pos);
	void renderObjects();
	void updateObjects(int deltaTime);
	void checkGround(const glm::ivec2 &pos, int animation);
	Object* getSword();
	void changeLevel();
	bool checkFinalDoor(const glm::ivec2 &pos, int *x);
	int checkPotions(const glm::ivec2 &pos);
	void getPotions(vector<int> *v1);

private:
	bool loadLevel(const string &levelFile);
	void prepareArrays(const glm::vec2 &minCoords, ShaderProgram &program);
	void changeToBrokenGround();
	

private:
	GLuint vao;
	GLuint vbo;
	GLint posLocation, texCoordLocation;
	glm::ivec2 position, mapSize, tilesheetSize;
	int tileSize, blockSize;
	Texture tilesheet;
	glm::vec2 tileTexSize;
	int *map, *jumpMatrix;

	vector<int> torchs, columns, thorns;
	int levelMap;
	string file;

	ShaderProgram shader;
	glm::vec2 coords;

	int screenX, screenY;

	//Portes
	vector<RelatedObjects> doors_pos;
	vector<AnimatedObject*> animDoors;
	
	//Terra trencat
	vector<Object*> ground_new;
	vector<BrokenGround> ground_pos;

	Object *sword;
	glm::ivec2 ground_changed;
	bool first = true;

	glm::ivec2 finalDoor_pos;
	glm::ivec2 finalDoor_activator;
	bool finalDoorOpened = false;
	AnimatedObject *finalDoor;

	vector<int> potions;
	Object *potion;

};


#endif // _TILE_MAP_INCLUDE


