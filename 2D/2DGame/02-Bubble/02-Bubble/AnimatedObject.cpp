#include "AnimatedObject.h"
#include <iostream>
#include <GL/glew.h>
#include <GL/glut.h>

enum object_anims {
	UP, DOWN, GOING_UP, GOING_DOWN
};

bool timeGotten = false;

void AnimatedObject::init(const glm::ivec2 &tileMapPos, ShaderProgram &shaderProgram) {
	spritesheet.loadFromFile(image, TEXTURE_PIXEL_FORMAT_RGBA);
	float aux = 1.0f / float(animations);
	sprite = Sprite::createSprite(glm::ivec2(tileSizeX, tileSizeY), glm::vec2(aux, 1.0f), &spritesheet, &shaderProgram);
	sprite->setNumberAnimations(animations);

	sprite->setAnimationSpeed(DOWN, speed);
	sprite->addKeyframe(DOWN, glm::vec2(0.f, 0.f));
	sprite->setSpeed(DOWN, glm::ivec2(0, 0), 1);

	sprite->setAnimationSpeed(UP, speed);
	sprite->addKeyframe(UP, glm::vec2(aux*(animations-1), 0.f));
	sprite->setSpeed(UP, glm::ivec2(0, 0), 1);

	sprite->setAnimationSpeed(GOING_UP, speed);
	for(int i = 1; i < animations; ++i) sprite->addKeyframe(GOING_UP, glm::vec2(i*aux, 0.f));
	sprite->setSpeed(GOING_UP, glm::ivec2(0, 0), animations);
	sprite->setLoop(GOING_UP, false);

	sprite->setAnimationSpeed(GOING_DOWN, speed);
	for (int i = 6; i >= 0; --i) sprite->addKeyframe(GOING_DOWN, glm::vec2(i*aux, 0.f));
	sprite->setSpeed(GOING_DOWN, glm::ivec2(0, 0), animations);
	sprite->setLoop(GOING_DOWN, false);

	time = 0;
	currentAnimation = DOWN;
	sprite->changeAnimation(DOWN);
	tileMapDispl = tileMapPos;
}

void AnimatedObject::update(int deltaTime) {
	if (currentAnimation == 2 && sprite->isFinished()) changeAnimation(0);
	else if (currentAnimation == 3 && sprite->isFinished()) changeAnimation(1);
	sprite->update(deltaTime);
}

void AnimatedObject::render() {
	sprite->render();
}

void AnimatedObject::setPosition(const glm::ivec2 &pos) {
	posObject = pos;
	sprite->setPosition(glm::ivec2(0, 0), posObject);
}

void AnimatedObject::setNumberAnimations(int n) {
	animations = n;
}

void AnimatedObject::setFile(const string file) {
	image = file;
}

void AnimatedObject::setTileSize(int sizeX, int sizeY) {
	tileSizeX = sizeX;
	tileSizeY = sizeY;
}

glm::ivec2 AnimatedObject::getPosition() {
	return posObject;
}

void AnimatedObject::changeAnimation(int anim) {
	if (currentAnimation != anim && sprite->isFinished()) {
		if (!(currentAnimation == 0 && anim == 2)) {
			currentAnimation = anim;
			sprite->changeAnimation(anim);
		}
	}
}

int AnimatedObject::getAnimation() {
	return currentAnimation;
}

bool AnimatedObject::animationFinished() {
	return sprite->isFinished();
}

void AnimatedObject::setSpeed(int s) {
	speed = s;
}