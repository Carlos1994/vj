#include "Object.h"
#include <iostream>
#include <GL/glew.h>
#include <GL/glut.h>

void Object::init(const glm::ivec2 &tileMapPos, ShaderProgram &shaderProgram) {
	spritesheet.loadFromFile(image, TEXTURE_PIXEL_FORMAT_RGBA);
	float aux = 1.0f / float(animations);
	sprite = Sprite::createSprite(glm::ivec2(tileSizeX, tileSizeY), glm::vec2(aux, 1.0f), &spritesheet, &shaderProgram);
	sprite->setNumberAnimations(animations);

	sprite->setAnimationSpeed(0, speed);
	for (int i = 0; i < animations; ++i) sprite->addKeyframe(0, glm::vec2(aux*i, 0.f));
	sprite->setSpeed(0, speed_vector, animations);

	sprite->setAnimationSpeed(1, speed);
	for (int i = 0; i < animations; ++i) sprite->addKeyframe(1, glm::vec2(aux*i, 0.f));
	sprite->setSpeed(1, glm::ivec2(0,0), animations);

	sprite->setAnimationSpeed(2, speed);
	sprite->addKeyframe(2, glm::vec2(0.f, 0.f));
	sprite->setSpeed(2, glm::ivec2(0, 0), 1);

	sprite->changeAnimation(0);
	tileMapDispl = tileMapPos;
	//sprite->setPosition(glm::ivec2(0,0), posObject);
}

void Object::update(int deltaTime) {
	sprite->update(deltaTime);
	sprite->setPosition(glm::ivec2(0, 0), posObject);
}

void Object::render() {
	if (visible) sprite->render();
}

void Object::setPosition(const glm::ivec2 &pos) {
	posObject = pos;
	sprite->setPosition(glm::ivec2(0,0), posObject);
	//printf("posx=%d, posy=%d\n", pos.x, pos.y);
}

void Object::setNumberAnimations(int n) {
	animations = n;
}

void Object::setFile(const string file) {
	image = file;
}

void Object::setTileSize(int sizeX, int sizeY) {
	tileSizeX = sizeX;
	tileSizeY = sizeY;
}

void Object::setSpeed(int speed1) {
	speed = speed1;
}

void Object::repeatAnimations(int times) {
	sprite->setNumberAnimations(animations*times);
	sprite->setAnimationSpeed(0, speed);
	float aux = 1.0f / float(animations);
	for (int i = 0; i < animations; ++i) 
		for (int j = 0; j < times; ++j) sprite->addKeyframe(0, glm::vec2(aux*i, 0.f));
	sprite->setSpeed(0, glm::ivec2(0, 0), animations*times);
}

bool Object::isFinished() {
	return sprite->isFinished();
}

void Object::setLoop(bool repeat) {
	sprite->setLoop(0, false);
}

void Object::setMovement(const glm::ivec2 &move) {
	speed_vector = move;
}

glm::ivec2 Object::getPosition() {
	return posObject;
}

void Object::setVisible(bool render) {
	visible = render;
}

void Object::changeAnimation(int anim) {
	sprite->changeAnimation(anim);
}