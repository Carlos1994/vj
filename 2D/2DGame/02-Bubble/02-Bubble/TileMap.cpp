#pragma comment(lib, "winmm.lib")
#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>
#include "TileMap.h"
#include <windows.h>
#include <mmsystem.h>


using namespace std;


TileMap *TileMap::createTileMap(const string &levelFile, const glm::vec2 &minCoords, ShaderProgram &program, int level)
{
	TileMap *map = new TileMap(levelFile, minCoords, program, level);
	
	return map;
}


TileMap::TileMap(const string &levelFile, const glm::vec2 &minCoords, ShaderProgram &program, int level)
{
	levelMap = level;
	file = levelFile;
	coords = minCoords;
	shader = program;
	loadLevel(levelFile);
	prepareArrays(minCoords, program);
}

TileMap::~TileMap()
{
	if (map != NULL)
		delete map;
	if (jumpMatrix != NULL)
		delete jumpMatrix;
	if (sword != NULL)
		delete sword;
	if (&animDoors != NULL){
		for (unsigned int i = 0; i < animDoors.size(); i++){
			delete animDoors[i];
		}
	}
	if (&ground_new != NULL){
		for (unsigned int i = 0; i < ground_new.size(); i++){
			delete ground_new[i];
		}
	}
	if (finalDoor != NULL)
		delete finalDoor;
}


void TileMap::render() const
{
	glEnable(GL_TEXTURE_2D);
	tilesheet.use();
	glBindVertexArray(vao);
	glEnableVertexAttribArray(posLocation);
	glEnableVertexAttribArray(texCoordLocation);
	glDrawArrays(GL_TRIANGLES, 0, 6 * mapSize.x * mapSize.y);
	glDisable(GL_TEXTURE_2D);
}

void TileMap::free()
{
	glDeleteBuffers(1, &vbo);
}

bool TileMap::loadLevel(const string &levelFile)
{
	ifstream fin;
	string line, tilesheetFile;
	stringstream sstream;
	char tile;
	
	fin.open(levelFile.c_str());
	if(!fin.is_open())
		return false;
	getline(fin, line);
	if(line.compare(0, 7, "TILEMAP") != 0)
		return false;
	getline(fin, line);
	sstream.str(line);
	sstream >> mapSize.x >> mapSize.y;
	getline(fin, line);
	sstream.str(line);
	sstream >> tileSize >> blockSize;
	getline(fin, line);
	sstream.str(line);
	sstream >> tilesheetFile;
	tilesheet.loadFromFile(tilesheetFile, TEXTURE_PIXEL_FORMAT_RGBA);
	tilesheet.setWrapS(GL_CLAMP_TO_EDGE);
	tilesheet.setWrapT(GL_CLAMP_TO_EDGE);
	tilesheet.setMinFilter(GL_NEAREST);
	tilesheet.setMagFilter(GL_NEAREST);
	getline(fin, line);
	sstream.str(line);
	sstream >> tilesheetSize.x >> tilesheetSize.y;
	tileTexSize = glm::vec2(1.f / tilesheetSize.x, 1.f / tilesheetSize.y);
	
	map = new int[mapSize.x * mapSize.y];
	torchs.clear();
	columns.clear();
	thorns.clear();
	bool first = true;
	for(int j=0; j<mapSize.y; j++)
	{
		for(int i=0; i<mapSize.x; i++)
		{
			fin.get(tile);
			first = true;
			while (tile != ' ') {
				if (first) {
					first = false;
					map[j*mapSize.x + i] = tile - int('0');
				}
				else {
					map[j*mapSize.x + i] *= 10;
					map[j*mapSize.x + i] += tile - int('0');
				}
				//Objects
				if (map[j*mapSize.x + i] == 11)	torchs.push_back(j*mapSize.x + i);
				else if (map[j*mapSize.x + i] == 12 || map[j*mapSize.x + i] == 5 || map[j*mapSize.x + i] == 31) columns.push_back(j*mapSize.x + i);
				else if (map[j*mapSize.x + i] == 16) thorns.push_back(j*mapSize.x + i);
				else if (map[j*mapSize.x + i] == 9) {
					ground_pos.resize(ground_pos.size() + 1);
					ground_pos[ground_pos.size() - 1].pos = glm::ivec2(i, j);
				}

				fin.get(tile);
			}
			/*if(tile == ' ')
				map[j*mapSize.x+i] = 0;
			else
				map[j*mapSize.x+i] = tile - int('0');*/
		}
		fin.get(tile);
#ifndef _WIN32
		fin.get(tile);
#endif
	}
	fin.get(tile);
	jumpMatrix = new int[mapSize.x * mapSize.y];
	for (int j = 0; j < mapSize.y; j++) {
		for (int i = 0; i < mapSize.x; i++) {
			jumpMatrix[j*mapSize.x + i] = tile - int('0');
			//printf("%d\n", tile - int('0'));
			fin.get(tile);
		}
		fin.get(tile);
	}
	//printf("%d\n\n\n", int(tile));

	//printf("Hola!!!!!!!");

	fin.get(tile);
	//printf("%c", tile);
	int n1 = tile - int('0');
	fin.get(tile);
	//printf("%c", tile);
	bool blank = false;
	if (tile == ' ') blank = true;
	else {
		n1 = (n1 * 10) + (tile - int('0'));
	}
	doors_pos.resize(n1);
	if (!blank) {
		fin.get(tile);
		//printf("%c", tile);
	}
	fin.get(tile);
	//printf("%c", tile);
	//printf("size doors = %d %d\n", n1, doors_pos.size());
	string number = "";
	int n, i;
	i = 0;
	first = true;
	while (int(tile) != 10) {
		if (tile == ' ') {
			n = atoi(number.c_str());
			if (first) {
				doors_pos[i].fElement.push_back(glm::ivec2(n % mapSize.x, n / mapSize.x));
				first = false;
			}
			else {
				doors_pos[i].sElement.push_back(glm::ivec2(n % mapSize.x, n / mapSize.x));
				first = true;
				++i;
			}
			number = "";
		}
		else {
			number += tile;
		}
		fin.get(tile);
	}

	glm::ivec2 sword_pos;
	fin.get(tile);
	sword_pos.x = tile - int('0');
	fin.get(tile);
	fin.get(tile);
	sword_pos.y = tile - int('0');

	number = "";
	fin.get(tile);
	while (tile != '.') {
		//printf("%c", tile);
		number += tile;
		fin.get(tile);		
	}
	finalDoor_pos = glm::ivec2();
	finalDoor_pos.x = atoi(number.c_str());
	//printf("%s %d\n", number.c_str(), atoi(number.c_str()));
	
	number = "";
	fin.get(tile);
	while (tile != '.') {
		//printf("%c", tile);
		number += tile;
		fin.get(tile);
	}
	finalDoor_pos.y = atoi(number.c_str());
	//printf("\n");

	finalDoor_activator = glm::ivec2();
	number = "";
	fin.get(tile);
	while (tile != '.') {
		//printf("%c", tile);
		number += tile;
		fin.get(tile);
	}
	finalDoor_activator.x = atoi(number.c_str());

	number = "";
	fin.get(tile);
	while (tile != '.') {
		//printf("%c", tile);
		number += tile;
		fin.get(tile);
	}
	finalDoor_activator.y = atoi(number.c_str());

	/*printf("Final door at %d %d\n", finalDoor_pos.x, finalDoor_pos.y);
	printf("Final door activator at %d %d\n", finalDoor_activator.x, finalDoor_activator.y);*/

	/*for (int i = 0; i < 4; ++i) {
		printf("door: %d,%d and floor: %d,%d \n", doors[i].fElement[0].x, doors[i].fElement[0].y, doors[i].sElement[0].x, doors[i].sElement[0].y);
	}*/

	/*for (int j = 0; j < mapSize.y; j++) {
		for (int i = 0; i < mapSize.x; i++) {
			//printf("%d ", jumpMatrix[mapSize.x*j + i]);
			if (map[j*mapSize.x + i] == 36) printf("Ground door at %d\n", j*mapSize.x + i);
		}
		//printf("\n");
	}*/
	
	fin.get(tile);
	fin.get(tile);
	number = "";
	potions = vector<int>();
	while (tile != ':') {
		if (tile != '.') number += tile;
		else {
			potions.push_back(atoi(number.c_str()));
			number = "";
		}
		fin.get(tile);
	}

	//for (int i = 0; i < potions.size(); ++i) printf("Potion at %d\n", potions[i]);


	fin.close();

	animDoors.resize(doors_pos.size());
	for (unsigned int i = 0; i < doors_pos.size(); ++i) {
		animDoors[i] = new AnimatedObject();
		animDoors[i]->setFile("images/door.png");
		animDoors[i]->setNumberAnimations(8);
		animDoors[i]->setTileSize(64, 64);
		animDoors[i]->init(glm::ivec2(0, 0), shader);
		animDoors[i]->setPosition(glm::ivec2(doors_pos[i].fElement[0].x * 32, doors_pos[i].fElement[0].y * 64));
	}
	
	ground_new.resize(ground_pos.size());
	for (unsigned int i = 0; i < ground_pos.size(); ++i) {
		//printf("Ground at %d %d\n", ground_pos[i].pos.y*mapSize.x + ground_pos[i].pos.x, ground_pos[i].visited);
		ground_new[i] = new Object();
		ground_new[i]->setFile("images/ground" + to_string(levelMap) + ".png");		
		ground_new[i]->setNumberAnimations(4);
		ground_new[i]->setTileSize(64, 64);
		ground_new[i]->setMovement(glm::ivec2(0, 3));
		ground_new[i]->init(glm::ivec2(0, 0), shader);
		ground_new[i]->setPosition(glm::ivec2(ground_pos[i].pos.x*32, ground_pos[i].pos.y * 64 - 3));
		ground_new[i]->changeAnimation(2);
	}

	/*ground = new Object();
	ground->setFile("images/ground.png");
	ground->setNumberAnimations(4);
	ground->setTileSize(64, 64);
	ground->setMovement(glm::ivec2(0, 3));
	ground->init(glm::ivec2(0, 0), shader);
	ground->setPosition(glm::ivec2(0, 0));
	//glm::ivec2 v = glm::ivec2(0, -4);*/

	sword = new Object();
	sword->setFile("images/sword.png");
	sword->setNumberAnimations(2);
	sword->setTileSize(32, 8);
	sword->setSpeed(1);
	sword->init(glm::ivec2(0, 0), shader);
	sword->setPosition(glm::ivec2(sword_pos.x * 32, sword_pos.y * 64 + 50));

	//Final door
	finalDoor = new AnimatedObject();
	finalDoor->setFile("images/final_door" + to_string(levelMap) + ".png");
	finalDoor->setNumberAnimations(11);
	finalDoor->setTileSize(64, 64);
	finalDoor->setSpeed(4);
	finalDoor->init(glm::ivec2(0, 0), shader);
	finalDoor->setPosition(glm::ivec2(finalDoor_pos.x * 32, finalDoor_pos.y * 64 - 16));

	return true;
}

void TileMap::prepareArrays(const glm::vec2 &minCoords, ShaderProgram &program)
{
	int tile, nTiles = 0;
	glm::vec2 posTile, texCoordTile[2], halfTexel;
	vector<float> vertices;
	
	halfTexel = glm::vec2(0.5f / tilesheet.width(), 0.5f / tilesheet.height());
	for (int j = 0; j<mapSize.y; j++)
	{
		for (int i = 0; i<mapSize.x; i++)
		{
			tile = map[j * mapSize.x + i];
			if(tile != 0)
			{
				// Non-empty tile
				nTiles++;
				posTile = glm::vec2(minCoords.x + i * 32, minCoords.y + j * tileSize);
				//texCoordTile[0] = glm::vec2(float((tile - 1) % tilesheetSize.x) / tilesheetSize.x, float((tile - 1) / tilesheetSize.y) / tilesheetSize.y);
				texCoordTile[0] = glm::vec2(float((tile - 1) % tilesheetSize.x) / tilesheetSize.x, float((tile - 1) / tilesheetSize.x) / tilesheetSize.y);
				texCoordTile[1] = texCoordTile[0] + tileTexSize;
				//texCoordTile[0] += halfTexel;
				texCoordTile[1] -= halfTexel;
				// First triangle
				vertices.push_back(posTile.x); vertices.push_back(posTile.y);
				vertices.push_back(texCoordTile[0].x); vertices.push_back(texCoordTile[0].y);
				vertices.push_back(posTile.x + blockSize); vertices.push_back(posTile.y);
				vertices.push_back(texCoordTile[1].x); vertices.push_back(texCoordTile[0].y);
				vertices.push_back(posTile.x + blockSize); vertices.push_back(posTile.y + blockSize);
				vertices.push_back(texCoordTile[1].x); vertices.push_back(texCoordTile[1].y);
				// Second triangle
				vertices.push_back(posTile.x); vertices.push_back(posTile.y);
				vertices.push_back(texCoordTile[0].x); vertices.push_back(texCoordTile[0].y);
				vertices.push_back(posTile.x + blockSize); vertices.push_back(posTile.y + blockSize);
				vertices.push_back(texCoordTile[1].x); vertices.push_back(texCoordTile[1].y);
				vertices.push_back(posTile.x); vertices.push_back(posTile.y + blockSize);
				vertices.push_back(texCoordTile[0].x); vertices.push_back(texCoordTile[1].y);
			}
		}
	}

	glGenVertexArrays(1, &vao);
	glBindVertexArray(vao);
	glGenBuffers(1, &vbo);
	glBindBuffer(GL_ARRAY_BUFFER, vbo);
	glBufferData(GL_ARRAY_BUFFER, 24 * nTiles * sizeof(float), &vertices[0], GL_STATIC_DRAW);
	posLocation = program.bindVertexAttribute("position", 2, 4*sizeof(float), 0);
	texCoordLocation = program.bindVertexAttribute("texCoord", 2, 4*sizeof(float), (void *)(2*sizeof(float)));
}

// Collision tests for axis aligned bounding boxes.
// Method collisionMoveDown also corrects Y coordinate if the box is
// already intersecting a tile below.

bool TileMap::collisionMoveLeft(const glm::ivec2 &pos, const glm::ivec2 &size) const
{
	int x, y0, y1;
	
	x = (pos.x+20) / 32;
	y0 = (pos.y + 32) / tileSize;
	y1 = (pos.y + 64 - 10) / tileSize;
	for(int y=y0; y<=y1; y++)
	{
		if (map[y*mapSize.x + x] == 6 || map[y*mapSize.x + x] == 7 || map[y*mapSize.x + x] == 18 || map[y*mapSize.x + x] == 15
			|| map[y*mapSize.x + x] == 29 || map[y*mapSize.x + x] == 30){

			//printf("COLIZION ESK %d %d \n", y, x);
			return true;
		}
		else if (map[y*mapSize.x + x] == 1) {
			for (unsigned int i = 0; i < doors_pos.size(); ++i) {
				if (doors_pos[i].fElement[0].x == x && doors_pos[i].fElement[0].y == y && (animDoors[i]->getAnimation() == 1 || (animDoors[i]->getAnimation() == 2 && !animDoors[i]->animationFinished()))) {
					return true;
				}
			}
		}
	}
	
	return false;
}

bool TileMap::collisionMoveRight(const glm::ivec2 &pos, const glm::ivec2 &size) const
{
	int x, y0, y1;
	
	x = (pos.x + 40) / 32;
	y0 = (pos.y + 32) / tileSize;
	y1 = (pos.y + 64 - 10) / tileSize;
	for(int y=y0; y<=y1; y++)
	{
		if (map[y*mapSize.x + x] == 6 || map[y*mapSize.x + x] == 7 || map[y*mapSize.x + x] == 18 || map[y*mapSize.x + x] == 15
			|| map[y*mapSize.x + x] == 29 || map[y*mapSize.x + x] == 30){

			//printf("COLIZION DRE\n");
			return true;
		}
		else if (map[y*mapSize.x + x] == 1) {
			for (unsigned int i = 0; i < doors_pos.size(); ++i) {
				if (doors_pos[i].fElement[0].x == x && doors_pos[i].fElement[0].y == y && (animDoors[i]->getAnimation() == 1 || (animDoors[i]->getAnimation()==2 && !animDoors[i]->animationFinished())) && ((pos.x+40) %32) > 20) {
					return true;
				}
			}
		}
	}
	
	return false;
}

bool TileMap::collisionMoveDown(const glm::ivec2 &pos, const glm::ivec2 &size, int *posY)
{
	/*int x0, x1, y;
	
	x0 = pos.x / 32;
	x1 = (pos.x + size.x - 1) / 32;
	y = (pos.y + size.y - 1) / tileSize;
	//printf("x0=%d, x1=%d, y=%d, posx=%d, posy=%d\n", x0, x1, y, pos.x, pos.y);
	for(int x=x0; x<=x1; x++)
	{
		printf("for\n");
		if(map[y*mapSize.x+x] != 0)
		{
			printf("First if\n");
			if(*posY - tileSize * y + size.y <= 4)
			{
				printf("Second if");
				*posY = tileSize * y - size.y;
				//printf("AAAH\n");
				return true;
			}
		}
	}*/

	int tilex = (pos.x+30) / 32;
	int tiley = (pos.y + 64) / 64;
	int tile_pj = map[mapSize.x*(tiley)+tilex];
	//printf("TILEEEEE %d  %d \n", tile_pj, pos.y);
	if ((!(tile_pj == 14 || tile_pj == 2 || tile_pj == 13 || tile_pj == 28/* || tile_pj == 9*/)) && (pos.y + 64) % 64 >= 58){
		//printf("ZUPA COLIZIONHGNH\n");
		*posY -= 3;
		return true;
	}
	return false;
}

bool TileMap::collisionMoveUp(const glm::ivec2 &pos, const glm::ivec2 &size, int *climb, bool &center) const {
	int tilex = (pos.x + 32) / 32;
	int tiley = (pos.y -1) / 64;
	int tile_pj = map[mapSize.x*(tiley)+tilex];
	//printf("posx=%d, posy=%d tilex=%d, tiley=%d\n", pos.x, pos.y, tilex+1, tiley);
	/*if ((map[mapSize.x*tiley + tilex - 1] == 2) || (map[mapSize.x*tiley + tilex + 1] == 2)){
		return false;
	}*/
	*climb = 0;
	//printf("Matrix=%d\n", jumpMatrix[mapSize.x*(tiley) + tilex+1]);

	//printf("TILE : %d \n", tile_pj);
	if (jumpMatrix[mapSize.x*tiley + tilex+2] != 0 && (pos.x %32 > 10)){
		center = true;
		//printf("CENTER TRUE MIRANT POS %d %d POS PLAYER %d %d: \n", tilex+2, tiley, pos.x, pos.y);
	}
	else{
		center = false;
		//printf("CENTER FALSE MIRANT POS %d %d POS PLAYER %d %d : \n",tilex + 2, tiley, pos.x, pos.y);
	}
	
	if (jumpMatrix[mapSize.x*tiley + tilex + 1] == 2) {
		if (map[mapSize.x*(tiley)+tilex + 1] == 1) {
			for (unsigned int i = 0; i < doors_pos.size(); ++i) {
				if (doors_pos[i].fElement[0].x == tilex + 1 && doors_pos[i].fElement[0].y == tiley && animDoors[i]->getAnimation() == 0) {
					*climb = 2;
				}
			}
		}
		else if (map[mapSize.x*(tiley)+tilex] != 9) *climb = 2;
		return false;
	}
	else if (jumpMatrix[mapSize.x*tiley+ tilex] == 1) {
		if (map[mapSize.x*(tiley)+tilex - 1] == 1) {
			for (unsigned int i = 0; i < doors_pos.size(); ++i) {
				if (doors_pos[i].fElement[0].x == tilex - 1 && doors_pos[i].fElement[0].y == tiley && animDoors[i]->getAnimation() == 0) {
					*climb = 1;
				}
			}
		}
		else if (map[mapSize.x*(tiley)+tilex] != 9) *climb = 1;
		return false;
	}
	else if ((tile_pj != 2 && tile_pj != 14 && tile_pj != 28) && ((pos.y+16)%64<=4)) {
		//printf("pos jugador : %d %d  tile que mirem : %d %d\n", pos.x, pos.y, tilex, tiley);
		//*posY += 4;
		if (tiley % 3 == 0) return false;
		//printf("COLIZION UP \n");
		return true;
	}
	return false;
}

void TileMap::getObjects(vector<int> *v1, vector<int> *v2, vector<int> *v3, vector<int> *v4) {
	*v1 = torchs;
	*v2 = columns;
	*v3 = thorns;
	*v4 = potions;
}

string TileMap::getLevel(){
	return file;
}

void TileMap::changeLevel(){
	++levelMap;
	file = "levels/level" + to_string(levelMap) + ".txt";
	//printf("file to load: %s\n", file.c_str());
	doors_pos.clear();
	animDoors.clear();
	ground_pos.clear();
	ground_new.clear();
	loadLevel(file);
	prepareArrays(coords, shader);
	render();
}

void TileMap::setScreen(int posX, int posY) {
	screenX = posX + 32;
	screenY = posY;
}

glm::ivec2 TileMap::getScreen() {
	return glm::ivec2(screenX, screenY);
}

void TileMap::checkScreen(const glm::ivec2 &pos) {
	int tilex = (pos.x + 32) / 32;
	int tiley = (pos.y+64) / 64;
	int tile_pj = map[mapSize.x*(tiley)+tilex];

	//printf("tilex: %d, tiley: %d\n", tilex, tiley);
	//printf("Player is at tile %d\n", tile_pj);
	//printf("Screen should be %dx%d\n", ((((tilex) * 32)+319)/320) * 320, ((((tiley) * 64)+192)/192)*192);

	setScreen(((((tilex)* 32) + 319) / 320) * 320, ((((tiley)* 64) + 192) / 192) * 192);
}

bool TileMap::cameraOnPos(const glm::ivec2 &cam, const glm::ivec2 &posEnemie){
	int tilex = (posEnemie.x + 32) / 32;
	int tiley = (posEnemie.y + 64) / 64;
	int tile_pj = map[mapSize.x*(tiley)+tilex];
	int camInix = ((((tilex * 32) + 319) / 320) * 320) + 32;
	int camIniy = ((((tiley * 64) + 192) / 192) * 192);
		bool temp=  ( camInix== cam.x && camIniy == cam.y);
	//printf("tilex: %d, tiley: %d\n", tilex, tiley);
	//printf("Player is at tile %d\n", tile_pj);
	//printf("Screen should be %dx%d\n", ((((tilex) * 32)+319)/320) * 320, ((((tiley) * 64)+192)/192)*192);
	//printf("CAMERA A : %d %d  CAMERA DEL ENEMIC A : %d %d \n", cam.x, cam.y, camInix, camIniy  );

	return temp;
}

glm::ivec2 TileMap::getMapSize() {
	return mapSize;
}

void TileMap::checkDoors(const glm::ivec2 &pos){
	int tilex = (pos.x + 20) / 32;
	int tiley = (pos.y + 64) / 64;
	int tilePlayer = map[mapSize.x*(tiley)+tilex];
	int x, y;
	
	for (unsigned int i = 0; i < doors_pos.size(); ++i) {
		if (!doors_pos[i].open && doors_pos[i].sElement[0].x == tilex && doors_pos[i].sElement[0].y == tiley && (pos.y + 64) % 64 >= 54)  {
			animDoors[i]->changeAnimation(2);
			animDoors[i]->render();
			/*printf("Player %d\n", tiley * 91 + tilex);
			printf("Door %d\n", doors_pos[i].fElement[0].x + 91*doors_pos[i].fElement[0].y);
			printf("A door has to be opened\n");*/
			PlaySound(TEXT("sounds/door_open.wav"), NULL, SND_FILENAME | SND_ASYNC);
			doors_pos[i].open = true;
		}
	}

	if (tilex == finalDoor_activator.x && tiley == finalDoor_activator.y && !finalDoorOpened) {
		finalDoorOpened = true;
		finalDoor->changeAnimation(2);
	}
	
	x = ground_changed.x % mapSize.x;
	y = ground_changed.x / mapSize.x;
	if ((tilePlayer == 17 || tilePlayer == 19) && (pos.y + 64) % 64 >= 54) {
		first = false;
		ground_changed.x = mapSize.x*(tiley)+tilex;
		ground_changed.y = tilePlayer;
		map[mapSize.x*(tiley)+tilex] = 20;
		prepareArrays(coords, shader);
	}
	else if (tilePlayer == 10 && (pos.y + 64) % 64 >= 54) {
		first = false;
		ground_changed.x = mapSize.x*(tiley)+tilex;
		ground_changed.y = tilePlayer;
		map[mapSize.x*(tiley)+tilex] = 21;
		prepareArrays(coords, shader);
	}
	else if ((tilePlayer != 21 || tilePlayer != 20) && (tilex != x || tiley != y) && !first) {
		map[mapSize.x*(y)+x] = ground_changed.y;
		first = true;
		prepareArrays(coords, shader);
	}
}

/*bool visited = false;
unsigned long time = 0;
glm::ivec2 ground_visited = glm::ivec2(99, 99);
bool change = false;*/

void TileMap::renderObjects() {
	/*for (unsigned int i = 0; i < ground_pos.size(); ++i) {
		//ground->setPosition(glm::ivec2(x*32, y*64));
		ground->render();
	}*/

	//Ground
	for (unsigned int i = 0; i < ground_pos.size(); ++i) {
		/*if (ground_pos[i].visited)*/
			ground_new[i]->render();
	}

	//Portes
	for (unsigned int i = 0; i < doors_pos.size(); ++i) {
		animDoors[i]->render();
	}

	finalDoor->render();
}

void TileMap::updateObjects(int deltaTime) {
	//Ground
	for (unsigned int i = 0; i < ground_pos.size(); ++i) {
		/*if (ground_pos[i].broken)*/ ground_new[i]->update(deltaTime);
	}
	changeToBrokenGround();

	//Portes
	for (unsigned int i = 0; i < doors_pos.size(); ++i) {
		animDoors[i]->update(deltaTime);
	}
	
	finalDoor->update(deltaTime);
}

int TileMap::getTile(glm::ivec2 pos){
	int tilex = (pos.x + 16 ) / 32;
	int tiley = (pos.y + 32) / 64;
	int tilePlayer = map[mapSize.x*(tiley)+tilex];
	return tilePlayer;
}

void TileMap::checkGround(const glm::ivec2 &pos, int animation) {
	int tilex = (pos.x + 20) / 32;
	int tiley = (pos.y + 64) / 64;
	int tilePlayer = map[mapSize.x*(tiley)+tilex];

	//printf("tile UP %d PosYmod %d\n\n\n", map[mapSize.x*(tiley - 1) + tilex], (pos.y + 16) % 64);
	bool animCorrect = (animation == 17 || animation == 16);
	if ((tilePlayer == 9 && (64 - ((pos.y + 64) % 64) <= 10)) || (((tiley-1)>=0) && map[mapSize.x*(tiley - 1) + tilex] == 9 && ((pos.y + 16) % 64 <= 4)) && animCorrect) {
		//printf("Distance: %d\n", ((pos.y + 64) % 64));
		/*if (!visited) {
			time = glutGet(GLUT_ELAPSED_TIME);
			ground_visited.x = tilex;
			ground_visited.y = tiley;
			visited = true;
		}*/

		int x, y;
		for (unsigned int i = 0; i < ground_pos.size(); ++i) {
			x = ground_pos[i].pos.x;
			y = ground_pos[i].pos.y;
			if (!ground_pos[i].visited && x == tilex && y == tiley) {
				ground_pos[i].visited = true;
				PlaySound(TEXT("sounds/moving_floor.wav"), NULL, SND_FILENAME | SND_ASYNC);
				ground_pos[i].time = glutGet(GLUT_ELAPSED_TIME);
				ground_new[i]->changeAnimation(1);
			}
			else if (!ground_pos[i].visited && x == tilex && y == (tiley - 1)) {
				ground_pos[i].visited = true;
				PlaySound(TEXT("sounds/moving_floor.wav"), NULL, SND_FILENAME | SND_ASYNC);
				ground_pos[i].time = glutGet(GLUT_ELAPSED_TIME);
				ground_new[i]->changeAnimation(1);
			}
		}
	}

	for (unsigned int i = 0; i < ground_pos.size(); ++i) {
		if (ground_pos[i].visited && !ground_pos[i].broken && ((glutGet(GLUT_ELAPSED_TIME) - ground_pos[i].time) > 2000)) {
			
			ground_new[i]->changeAnimation(0);

			ground_pos[i].broken = true;
			int left_down = map[mapSize.x*(ground_pos[i].pos.y + 1) + ground_pos[i].pos.x -1];
			int left = map[mapSize.x*(ground_pos[i].pos.y) + ground_pos[i].pos.x - 1];
			map[mapSize.x*(ground_pos[i].pos.y) + ground_pos[i].pos.x] = 2;
			if (left_down == 15 || left_down == 18 || left_down == 7) {
				if (left == 5 || left == 12) {
					map[mapSize.x*(ground_pos[i].pos.y) + ground_pos[i].pos.x - 1] = 12;
				}
				else if (left != 19 && left != 11) map[mapSize.x*(ground_pos[i].pos.y) + ground_pos[i].pos.x - 1] = 20;
			}
			else if (left_down == 5 || left_down == 12) {
				if (left == 5 || left == 12) {
					map[mapSize.x*(ground_pos[i].pos.y) + ground_pos[i].pos.x - 1] = 5;
				}
				else map[mapSize.x*(ground_pos[i].pos.y) + ground_pos[i].pos.x - 1] = 21;
			}
			prepareArrays(coords, shader);
		}
	}
	

	/*if (((glutGet(GLUT_ELAPSED_TIME) - time) > 2000) && visited) {
		visited = false;
		int left_down = map[mapSize.x*(ground_visited.y+1) + ground_visited.x-1];
		int left = map[mapSize.x*(ground_visited.y) + ground_visited.x-1];
		map[mapSize.x*(ground_visited.y)+ground_visited.x] = 2;
		if (left_down == 15 || left_down == 18) {
			if (left == 5) {
				map[mapSize.x*(ground_visited.y) + ground_visited.x - 1] = 12;
			}
			else map[mapSize.x*(ground_visited.y) + ground_visited.x - 1] = 20;
		}
		else if (left_down == 5 || left_down == 12) {
			if (left == 5 || left == 12) {
				map[mapSize.x*(ground_visited.y) + ground_visited.x - 1] = 5;
			}
			else map[mapSize.x*(ground_visited.y) + ground_visited.x - 1] = 10;
		}
		prepareArrays(coords, shader);
		ground->setPosition(glm::ivec2(ground_visited.x * 32, ground_visited.y * 64));
		ground->setVisible(true);
		change = true;
	}*/
}

void TileMap::changeToBrokenGround() {
	/*int tiley = (pos.y + 1) / 64;
	int tileGround = map[mapSize.x*tiley + (pos.x / 32)];
	printf("tileeeeeeeeeeeeeeeeeeeeeeeee %d\n\n", map[mapSize.x*tiley + (pos.x / 32)]);
	if (((pos.y + 1) % 64 <= 4) && (!(tileGround == 14 || tileGround == 2 || tileGround == 13))) {
		//printf("%d\n\n", pos.y);
		//printf("tile %d\n\n", map[mapSize.x*tiley + (pos.x / 32)]);
		map[mapSize.x*tiley + (pos.x / 32)] = 8;
		prepareArrays(coords, shader);
		//ground->setVisible(false);
		//change = false;
	}*/
	int tiley, tileGround;
	glm::ivec2 gPos;
	for (unsigned int i = 0; i < ground_pos.size(); ++i) {
		if (ground_pos[i].broken) {
			gPos = ground_new[i]->getPosition();
			tiley = (gPos.y + 1) / 64;
			tileGround = map[mapSize.x*tiley + (gPos.x / 32)];
			if (((gPos.y + 1) % 64 <= 4) && (!(tileGround == 14 || tileGround == 2 || tileGround == 13))) {
				map[mapSize.x*tiley + (gPos.x / 32)] = 8;
				prepareArrays(coords, shader);
				PlaySound(TEXT("sounds/crashing_floor.wav"), NULL, SND_FILENAME | SND_ASYNC);
				ground_new.erase(ground_new.begin() + i);
				ground_pos.erase(ground_pos.begin() + i);
			}
		}
	}
}

Object* TileMap::getSword() {
	return sword;
}

int TileMap::getJumpTile(int x, int y){

	return jumpMatrix[mapSize.x*y + x];
}

int TileMap::getTileFromTile(glm::ivec2 tile){
	int tilePlayer = map[mapSize.x*(tile.y)+tile.x];
	return tilePlayer;
}

bool TileMap::checkFinalDoor(const glm::ivec2 &pos, int *x) {
	int tilex = (pos.x + 20) / 32;
	int tiley = (pos.y + 64) / 64;

	*x = finalDoor_pos.x;
	return (tilex == finalDoor_pos.x && tiley == finalDoor_pos.y && finalDoor->getAnimation() == 0 && finalDoor->animationFinished());
}

int TileMap::checkPotions(const glm::ivec2 &pos) {
	int tilex = (pos.x + 20) / 32;
	int tiley = (pos.y + 64) / 64;

	int potionX;

	for (unsigned int i = 0; i < potions.size(); ++i) {
		potionX = (potions[i] % mapSize.x) * 32;

		//if (i == 0) printf("tilePot %d tiellex %d calcul %d\n", /*abs(potionX - pos.x - 20)*/potions[i] % mapSize.x, tilex, abs(potionX - pos.x - 20));

		if (tiley == (potions[i] / mapSize.x) && (tilex == (potions[i] % mapSize.x) || abs(potionX - pos.x - 20) <= 10)) {
			//printf("PO ME LA BEBO TETE!\n\n");
			potions.erase(potions.begin() + i);
			return (potionX / 32);
		}
	}
	//printf("Hoy vaze que no!\n");
	return -1;
}


void TileMap::getPotions(vector<int> *v1) {
	*v1 = potions;
}


























